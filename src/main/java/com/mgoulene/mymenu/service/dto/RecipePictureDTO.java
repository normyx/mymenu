package com.mgoulene.mymenu.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.mgoulene.mymenu.domain.RecipePicture} entity.
 */
public class RecipePictureDTO implements Serializable {

    private Long id;

    @Lob
    private byte[] picture;

    private String pictureContentType;

    @Size(max = 400)
    private String label;

    @NotNull
    @Min(value = 1)
    private Integer order;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getPictureContentType() {
        return pictureContentType;
    }

    public void setPictureContentType(String pictureContentType) {
        this.pictureContentType = pictureContentType;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RecipePictureDTO)) {
            return false;
        }

        RecipePictureDTO recipePictureDTO = (RecipePictureDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, recipePictureDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RecipePictureDTO{" +
            "id=" + getId() +
            ", picture='" + getPicture() + "'" +
            ", label='" + getLabel() + "'" +
            ", order=" + getOrder() +
            "}";
    }
}

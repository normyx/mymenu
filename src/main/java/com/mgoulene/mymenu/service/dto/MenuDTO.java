package com.mgoulene.mymenu.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.mgoulene.mymenu.domain.Menu} entity.
 */
public class MenuDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate date;

    @NotNull
    private String userId;

    private RecipeDTO starter;

    private RecipeDTO main;

    private RecipeDTO dessert;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public RecipeDTO getStarter() {
        return starter;
    }

    public void setStarter(RecipeDTO starter) {
        this.starter = starter;
    }

    public RecipeDTO getMain() {
        return main;
    }

    public void setMain(RecipeDTO main) {
        this.main = main;
    }

    public RecipeDTO getDessert() {
        return dessert;
    }

    public void setDessert(RecipeDTO dessert) {
        this.dessert = dessert;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MenuDTO)) {
            return false;
        }

        MenuDTO menuDTO = (MenuDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, menuDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MenuDTO{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", userId='" + getUserId() + "'" +
            ", starter=" + getStarter() +
            ", main=" + getMain() +
            ", dessert=" + getDessert() +
            "}";
    }
}

package com.mgoulene.mymenu.service;

import static org.elasticsearch.index.query.QueryBuilders.*;

import com.mgoulene.mymenu.domain.RecipePicture;
import com.mgoulene.mymenu.repository.RecipePictureRepository;
import com.mgoulene.mymenu.repository.search.RecipePictureSearchRepository;
import com.mgoulene.mymenu.service.dto.RecipePictureDTO;
import com.mgoulene.mymenu.service.mapper.RecipePictureMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link RecipePicture}.
 */
@Service
@Transactional
public class RecipePictureService {

    private final Logger log = LoggerFactory.getLogger(RecipePictureService.class);

    private final RecipePictureRepository recipePictureRepository;

    private final RecipePictureMapper recipePictureMapper;

    private final RecipePictureSearchRepository recipePictureSearchRepository;

    public RecipePictureService(
        RecipePictureRepository recipePictureRepository,
        RecipePictureMapper recipePictureMapper,
        RecipePictureSearchRepository recipePictureSearchRepository
    ) {
        this.recipePictureRepository = recipePictureRepository;
        this.recipePictureMapper = recipePictureMapper;
        this.recipePictureSearchRepository = recipePictureSearchRepository;
    }

    /**
     * Save a recipePicture.
     *
     * @param recipePictureDTO the entity to save.
     * @return the persisted entity.
     */
    public RecipePictureDTO save(RecipePictureDTO recipePictureDTO) {
        log.debug("Request to save RecipePicture : {}", recipePictureDTO);
        RecipePicture recipePicture = recipePictureMapper.toEntity(recipePictureDTO);
        recipePicture = recipePictureRepository.save(recipePicture);
        RecipePictureDTO result = recipePictureMapper.toDto(recipePicture);
        recipePictureSearchRepository.save(recipePicture);
        return result;
    }

    /**
     * Partially update a recipePicture.
     *
     * @param recipePictureDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<RecipePictureDTO> partialUpdate(RecipePictureDTO recipePictureDTO) {
        log.debug("Request to partially update RecipePicture : {}", recipePictureDTO);

        return recipePictureRepository
            .findById(recipePictureDTO.getId())
            .map(existingRecipePicture -> {
                recipePictureMapper.partialUpdate(existingRecipePicture, recipePictureDTO);

                return existingRecipePicture;
            })
            .map(recipePictureRepository::save)
            .map(savedRecipePicture -> {
                recipePictureSearchRepository.save(savedRecipePicture);

                return savedRecipePicture;
            })
            .map(recipePictureMapper::toDto);
    }

    /**
     * Get all the recipePictures.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<RecipePictureDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RecipePictures");
        return recipePictureRepository.findAll(pageable).map(recipePictureMapper::toDto);
    }

    /**
     * Get one recipePicture by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RecipePictureDTO> findOne(Long id) {
        log.debug("Request to get RecipePicture : {}", id);
        return recipePictureRepository.findById(id).map(recipePictureMapper::toDto);
    }

    /**
     * Delete the recipePicture by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RecipePicture : {}", id);
        recipePictureRepository.deleteById(id);
        recipePictureSearchRepository.deleteById(id);
    }

    /**
     * Search for the recipePicture corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<RecipePictureDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of RecipePictures for query {}", query);
        return recipePictureSearchRepository.search(query, pageable).map(recipePictureMapper::toDto);
    }
}

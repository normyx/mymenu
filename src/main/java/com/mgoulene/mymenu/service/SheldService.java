package com.mgoulene.mymenu.service;

import static org.elasticsearch.index.query.QueryBuilders.*;

import com.mgoulene.mymenu.domain.Sheld;
import com.mgoulene.mymenu.repository.SheldRepository;
import com.mgoulene.mymenu.repository.search.SheldSearchRepository;
import com.mgoulene.mymenu.service.dto.SheldDTO;
import com.mgoulene.mymenu.service.mapper.SheldMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Sheld}.
 */
@Service
@Transactional
public class SheldService {

    private final Logger log = LoggerFactory.getLogger(SheldService.class);

    private final SheldRepository sheldRepository;

    private final SheldMapper sheldMapper;

    private final SheldSearchRepository sheldSearchRepository;

    public SheldService(SheldRepository sheldRepository, SheldMapper sheldMapper, SheldSearchRepository sheldSearchRepository) {
        this.sheldRepository = sheldRepository;
        this.sheldMapper = sheldMapper;
        this.sheldSearchRepository = sheldSearchRepository;
    }

    /**
     * Save a sheld.
     *
     * @param sheldDTO the entity to save.
     * @return the persisted entity.
     */
    public SheldDTO save(SheldDTO sheldDTO) {
        log.debug("Request to save Sheld : {}", sheldDTO);
        Sheld sheld = sheldMapper.toEntity(sheldDTO);
        sheld = sheldRepository.save(sheld);
        SheldDTO result = sheldMapper.toDto(sheld);
        sheldSearchRepository.save(sheld);
        return result;
    }

    /**
     * Partially update a sheld.
     *
     * @param sheldDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<SheldDTO> partialUpdate(SheldDTO sheldDTO) {
        log.debug("Request to partially update Sheld : {}", sheldDTO);

        return sheldRepository
            .findById(sheldDTO.getId())
            .map(existingSheld -> {
                sheldMapper.partialUpdate(existingSheld, sheldDTO);

                return existingSheld;
            })
            .map(sheldRepository::save)
            .map(savedSheld -> {
                sheldSearchRepository.save(savedSheld);

                return savedSheld;
            })
            .map(sheldMapper::toDto);
    }

    /**
     * Get all the shelds.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SheldDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Shelds");
        return sheldRepository.findAll(pageable).map(sheldMapper::toDto);
    }

    /**
     * Get one sheld by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SheldDTO> findOne(Long id) {
        log.debug("Request to get Sheld : {}", id);
        return sheldRepository.findById(id).map(sheldMapper::toDto);
    }

    /**
     * Delete the sheld by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Sheld : {}", id);
        sheldRepository.deleteById(id);
        sheldSearchRepository.deleteById(id);
    }

    /**
     * Search for the sheld corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SheldDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Shelds for query {}", query);
        return sheldSearchRepository.search(query, pageable).map(sheldMapper::toDto);
    }
}

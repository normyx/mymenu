package com.mgoulene.mymenu.service.mapper;

import com.mgoulene.mymenu.domain.RecipeStep;
import com.mgoulene.mymenu.service.dto.RecipeStepDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link RecipeStep} and its DTO {@link RecipeStepDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RecipeStepMapper extends EntityMapper<RecipeStepDTO, RecipeStep> {
    @Named("stepDescriptionSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "stepDescription", source = "stepDescription")
    Set<RecipeStepDTO> toDtoStepDescriptionSet(Set<RecipeStep> recipeStep);
}

package com.mgoulene.mymenu.service.mapper;

import com.mgoulene.mymenu.domain.Ingredient;
import com.mgoulene.mymenu.service.dto.IngredientDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Ingredient} and its DTO {@link IngredientDTO}.
 */
@Mapper(componentModel = "spring", uses = { ProductMapper.class })
public interface IngredientMapper extends EntityMapper<IngredientDTO, Ingredient> {
    @Mapping(target = "product", source = "product", qualifiedByName = "name")
    IngredientDTO toDto(Ingredient s);

    @Named("quantityTypeSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "quantityType", source = "quantityType")
    Set<IngredientDTO> toDtoQuantityTypeSet(Set<Ingredient> ingredient);
}

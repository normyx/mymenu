package com.mgoulene.mymenu.service.mapper;

import com.mgoulene.mymenu.domain.Product;
import com.mgoulene.mymenu.service.dto.ProductDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Product} and its DTO {@link ProductDTO}.
 */
@Mapper(componentModel = "spring", uses = { SheldMapper.class })
public interface ProductMapper extends EntityMapper<ProductDTO, Product> {
    @Mapping(target = "sheld", source = "sheld", qualifiedByName = "name")
    ProductDTO toDto(Product s);

    @Named("name")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    ProductDTO toDtoName(Product product);
}

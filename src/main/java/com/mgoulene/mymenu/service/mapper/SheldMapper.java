package com.mgoulene.mymenu.service.mapper;

import com.mgoulene.mymenu.domain.Sheld;
import com.mgoulene.mymenu.service.dto.SheldDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Sheld} and its DTO {@link SheldDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SheldMapper extends EntityMapper<SheldDTO, Sheld> {
    @Named("name")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    SheldDTO toDtoName(Sheld sheld);
}

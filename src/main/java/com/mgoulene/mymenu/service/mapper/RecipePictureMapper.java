package com.mgoulene.mymenu.service.mapper;

import com.mgoulene.mymenu.domain.RecipePicture;
import com.mgoulene.mymenu.service.dto.RecipePictureDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link RecipePicture} and its DTO {@link RecipePictureDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RecipePictureMapper extends EntityMapper<RecipePictureDTO, RecipePicture> {
    @Named("labelSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "label", source = "label")
    Set<RecipePictureDTO> toDtoLabelSet(Set<RecipePicture> recipePicture);
}

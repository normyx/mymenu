package com.mgoulene.mymenu.service;

import static org.elasticsearch.index.query.QueryBuilders.*;

import com.mgoulene.mymenu.domain.RecipeStep;
import com.mgoulene.mymenu.repository.RecipeStepRepository;
import com.mgoulene.mymenu.repository.search.RecipeStepSearchRepository;
import com.mgoulene.mymenu.service.dto.RecipeStepDTO;
import com.mgoulene.mymenu.service.mapper.RecipeStepMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link RecipeStep}.
 */
@Service
@Transactional
public class RecipeStepService {

    private final Logger log = LoggerFactory.getLogger(RecipeStepService.class);

    private final RecipeStepRepository recipeStepRepository;

    private final RecipeStepMapper recipeStepMapper;

    private final RecipeStepSearchRepository recipeStepSearchRepository;

    public RecipeStepService(
        RecipeStepRepository recipeStepRepository,
        RecipeStepMapper recipeStepMapper,
        RecipeStepSearchRepository recipeStepSearchRepository
    ) {
        this.recipeStepRepository = recipeStepRepository;
        this.recipeStepMapper = recipeStepMapper;
        this.recipeStepSearchRepository = recipeStepSearchRepository;
    }

    /**
     * Save a recipeStep.
     *
     * @param recipeStepDTO the entity to save.
     * @return the persisted entity.
     */
    public RecipeStepDTO save(RecipeStepDTO recipeStepDTO) {
        log.debug("Request to save RecipeStep : {}", recipeStepDTO);
        RecipeStep recipeStep = recipeStepMapper.toEntity(recipeStepDTO);
        recipeStep = recipeStepRepository.save(recipeStep);
        RecipeStepDTO result = recipeStepMapper.toDto(recipeStep);
        recipeStepSearchRepository.save(recipeStep);
        return result;
    }

    /**
     * Partially update a recipeStep.
     *
     * @param recipeStepDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<RecipeStepDTO> partialUpdate(RecipeStepDTO recipeStepDTO) {
        log.debug("Request to partially update RecipeStep : {}", recipeStepDTO);

        return recipeStepRepository
            .findById(recipeStepDTO.getId())
            .map(existingRecipeStep -> {
                recipeStepMapper.partialUpdate(existingRecipeStep, recipeStepDTO);

                return existingRecipeStep;
            })
            .map(recipeStepRepository::save)
            .map(savedRecipeStep -> {
                recipeStepSearchRepository.save(savedRecipeStep);

                return savedRecipeStep;
            })
            .map(recipeStepMapper::toDto);
    }

    /**
     * Get all the recipeSteps.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<RecipeStepDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RecipeSteps");
        return recipeStepRepository.findAll(pageable).map(recipeStepMapper::toDto);
    }

    /**
     * Get one recipeStep by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RecipeStepDTO> findOne(Long id) {
        log.debug("Request to get RecipeStep : {}", id);
        return recipeStepRepository.findById(id).map(recipeStepMapper::toDto);
    }

    /**
     * Delete the recipeStep by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RecipeStep : {}", id);
        recipeStepRepository.deleteById(id);
        recipeStepSearchRepository.deleteById(id);
    }

    /**
     * Search for the recipeStep corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<RecipeStepDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of RecipeSteps for query {}", query);
        return recipeStepSearchRepository.search(query, pageable).map(recipeStepMapper::toDto);
    }
}

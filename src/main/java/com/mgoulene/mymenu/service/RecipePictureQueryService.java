package com.mgoulene.mymenu.service;

import com.mgoulene.mymenu.domain.*; // for static metamodels
import com.mgoulene.mymenu.domain.RecipePicture;
import com.mgoulene.mymenu.repository.RecipePictureRepository;
import com.mgoulene.mymenu.repository.search.RecipePictureSearchRepository;
import com.mgoulene.mymenu.service.criteria.RecipePictureCriteria;
import com.mgoulene.mymenu.service.dto.RecipePictureDTO;
import com.mgoulene.mymenu.service.mapper.RecipePictureMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link RecipePicture} entities in the database.
 * The main input is a {@link RecipePictureCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RecipePictureDTO} or a {@link Page} of {@link RecipePictureDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RecipePictureQueryService extends QueryService<RecipePicture> {

    private final Logger log = LoggerFactory.getLogger(RecipePictureQueryService.class);

    private final RecipePictureRepository recipePictureRepository;

    private final RecipePictureMapper recipePictureMapper;

    private final RecipePictureSearchRepository recipePictureSearchRepository;

    public RecipePictureQueryService(
        RecipePictureRepository recipePictureRepository,
        RecipePictureMapper recipePictureMapper,
        RecipePictureSearchRepository recipePictureSearchRepository
    ) {
        this.recipePictureRepository = recipePictureRepository;
        this.recipePictureMapper = recipePictureMapper;
        this.recipePictureSearchRepository = recipePictureSearchRepository;
    }

    /**
     * Return a {@link List} of {@link RecipePictureDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RecipePictureDTO> findByCriteria(RecipePictureCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RecipePicture> specification = createSpecification(criteria);
        return recipePictureMapper.toDto(recipePictureRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link RecipePictureDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RecipePictureDTO> findByCriteria(RecipePictureCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RecipePicture> specification = createSpecification(criteria);
        return recipePictureRepository.findAll(specification, page).map(recipePictureMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RecipePictureCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RecipePicture> specification = createSpecification(criteria);
        return recipePictureRepository.count(specification);
    }

    /**
     * Function to convert {@link RecipePictureCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RecipePicture> createSpecification(RecipePictureCriteria criteria) {
        Specification<RecipePicture> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), RecipePicture_.id));
            }
            if (criteria.getLabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabel(), RecipePicture_.label));
            }
            if (criteria.getOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getOrder(), RecipePicture_.order));
            }
            if (criteria.getRecipeId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getRecipeId(), root -> root.join(RecipePicture_.recipes, JoinType.LEFT).get(Recipe_.id))
                    );
            }
        }
        return specification;
    }
}

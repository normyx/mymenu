package com.mgoulene.mymenu.service.criteria;

import com.mgoulene.mymenu.domain.enumeration.QuantityType;
import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.mgoulene.mymenu.domain.Ingredient} entity. This class is used
 * in {@link com.mgoulene.mymenu.web.rest.IngredientResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /ingredients?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
public class IngredientCriteria implements Serializable, Criteria {

    /**
     * Class for filtering QuantityType
     */
    public static class QuantityTypeFilter extends Filter<QuantityType> {

        public QuantityTypeFilter() {}

        public QuantityTypeFilter(QuantityTypeFilter filter) {
            super(filter);
        }

        @Override
        public QuantityTypeFilter copy() {
            return new QuantityTypeFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private FloatFilter quantity;

    private QuantityTypeFilter quantityType;

    private LongFilter productId;

    private LongFilter recipeId;

    private Boolean distinct;

    public IngredientCriteria() {}

    public IngredientCriteria(IngredientCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.quantity = other.quantity == null ? null : other.quantity.copy();
        this.quantityType = other.quantityType == null ? null : other.quantityType.copy();
        this.productId = other.productId == null ? null : other.productId.copy();
        this.recipeId = other.recipeId == null ? null : other.recipeId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public IngredientCriteria copy() {
        return new IngredientCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public FloatFilter getQuantity() {
        return quantity;
    }

    public FloatFilter quantity() {
        if (quantity == null) {
            quantity = new FloatFilter();
        }
        return quantity;
    }

    public void setQuantity(FloatFilter quantity) {
        this.quantity = quantity;
    }

    public QuantityTypeFilter getQuantityType() {
        return quantityType;
    }

    public QuantityTypeFilter quantityType() {
        if (quantityType == null) {
            quantityType = new QuantityTypeFilter();
        }
        return quantityType;
    }

    public void setQuantityType(QuantityTypeFilter quantityType) {
        this.quantityType = quantityType;
    }

    public LongFilter getProductId() {
        return productId;
    }

    public LongFilter productId() {
        if (productId == null) {
            productId = new LongFilter();
        }
        return productId;
    }

    public void setProductId(LongFilter productId) {
        this.productId = productId;
    }

    public LongFilter getRecipeId() {
        return recipeId;
    }

    public LongFilter recipeId() {
        if (recipeId == null) {
            recipeId = new LongFilter();
        }
        return recipeId;
    }

    public void setRecipeId(LongFilter recipeId) {
        this.recipeId = recipeId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final IngredientCriteria that = (IngredientCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(quantity, that.quantity) &&
            Objects.equals(quantityType, that.quantityType) &&
            Objects.equals(productId, that.productId) &&
            Objects.equals(recipeId, that.recipeId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, quantity, quantityType, productId, recipeId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "IngredientCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (quantity != null ? "quantity=" + quantity + ", " : "") +
            (quantityType != null ? "quantityType=" + quantityType + ", " : "") +
            (productId != null ? "productId=" + productId + ", " : "") +
            (recipeId != null ? "recipeId=" + recipeId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}

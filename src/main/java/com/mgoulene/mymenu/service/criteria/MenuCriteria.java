package com.mgoulene.mymenu.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LocalDateFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.mgoulene.mymenu.domain.Menu} entity. This class is used
 * in {@link com.mgoulene.mymenu.web.rest.MenuResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /menus?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
public class MenuCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LocalDateFilter date;

    private StringFilter userId;

    private LongFilter starterId;

    private LongFilter mainId;

    private LongFilter dessertId;

    private Boolean distinct;

    public MenuCriteria() {}

    public MenuCriteria(MenuCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.date = other.date == null ? null : other.date.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.starterId = other.starterId == null ? null : other.starterId.copy();
        this.mainId = other.mainId == null ? null : other.mainId.copy();
        this.dessertId = other.dessertId == null ? null : other.dessertId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public MenuCriteria copy() {
        return new MenuCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LocalDateFilter getDate() {
        return date;
    }

    public LocalDateFilter date() {
        if (date == null) {
            date = new LocalDateFilter();
        }
        return date;
    }

    public void setDate(LocalDateFilter date) {
        this.date = date;
    }

    public StringFilter getUserId() {
        return userId;
    }

    public StringFilter userId() {
        if (userId == null) {
            userId = new StringFilter();
        }
        return userId;
    }

    public void setUserId(StringFilter userId) {
        this.userId = userId;
    }

    public LongFilter getStarterId() {
        return starterId;
    }

    public LongFilter starterId() {
        if (starterId == null) {
            starterId = new LongFilter();
        }
        return starterId;
    }

    public void setStarterId(LongFilter starterId) {
        this.starterId = starterId;
    }

    public LongFilter getMainId() {
        return mainId;
    }

    public LongFilter mainId() {
        if (mainId == null) {
            mainId = new LongFilter();
        }
        return mainId;
    }

    public void setMainId(LongFilter mainId) {
        this.mainId = mainId;
    }

    public LongFilter getDessertId() {
        return dessertId;
    }

    public LongFilter dessertId() {
        if (dessertId == null) {
            dessertId = new LongFilter();
        }
        return dessertId;
    }

    public void setDessertId(LongFilter dessertId) {
        this.dessertId = dessertId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MenuCriteria that = (MenuCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(date, that.date) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(starterId, that.starterId) &&
            Objects.equals(mainId, that.mainId) &&
            Objects.equals(dessertId, that.dessertId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, userId, starterId, mainId, dessertId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MenuCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (date != null ? "date=" + date + ", " : "") +
            (userId != null ? "userId=" + userId + ", " : "") +
            (starterId != null ? "starterId=" + starterId + ", " : "") +
            (mainId != null ? "mainId=" + mainId + ", " : "") +
            (dessertId != null ? "dessertId=" + dessertId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}

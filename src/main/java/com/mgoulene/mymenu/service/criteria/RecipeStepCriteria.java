package com.mgoulene.mymenu.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.mgoulene.mymenu.domain.RecipeStep} entity. This class is used
 * in {@link com.mgoulene.mymenu.web.rest.RecipeStepResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /recipe-steps?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
public class RecipeStepCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter stepNum;

    private StringFilter stepDescription;

    private LongFilter recipeId;

    private Boolean distinct;

    public RecipeStepCriteria() {}

    public RecipeStepCriteria(RecipeStepCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.stepNum = other.stepNum == null ? null : other.stepNum.copy();
        this.stepDescription = other.stepDescription == null ? null : other.stepDescription.copy();
        this.recipeId = other.recipeId == null ? null : other.recipeId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public RecipeStepCriteria copy() {
        return new RecipeStepCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getStepNum() {
        return stepNum;
    }

    public IntegerFilter stepNum() {
        if (stepNum == null) {
            stepNum = new IntegerFilter();
        }
        return stepNum;
    }

    public void setStepNum(IntegerFilter stepNum) {
        this.stepNum = stepNum;
    }

    public StringFilter getStepDescription() {
        return stepDescription;
    }

    public StringFilter stepDescription() {
        if (stepDescription == null) {
            stepDescription = new StringFilter();
        }
        return stepDescription;
    }

    public void setStepDescription(StringFilter stepDescription) {
        this.stepDescription = stepDescription;
    }

    public LongFilter getRecipeId() {
        return recipeId;
    }

    public LongFilter recipeId() {
        if (recipeId == null) {
            recipeId = new LongFilter();
        }
        return recipeId;
    }

    public void setRecipeId(LongFilter recipeId) {
        this.recipeId = recipeId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RecipeStepCriteria that = (RecipeStepCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(stepNum, that.stepNum) &&
            Objects.equals(stepDescription, that.stepDescription) &&
            Objects.equals(recipeId, that.recipeId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, stepNum, stepDescription, recipeId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RecipeStepCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (stepNum != null ? "stepNum=" + stepNum + ", " : "") +
            (stepDescription != null ? "stepDescription=" + stepDescription + ", " : "") +
            (recipeId != null ? "recipeId=" + recipeId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}

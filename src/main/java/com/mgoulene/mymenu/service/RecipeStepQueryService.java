package com.mgoulene.mymenu.service;

import com.mgoulene.mymenu.domain.*; // for static metamodels
import com.mgoulene.mymenu.domain.RecipeStep;
import com.mgoulene.mymenu.repository.RecipeStepRepository;
import com.mgoulene.mymenu.repository.search.RecipeStepSearchRepository;
import com.mgoulene.mymenu.service.criteria.RecipeStepCriteria;
import com.mgoulene.mymenu.service.dto.RecipeStepDTO;
import com.mgoulene.mymenu.service.mapper.RecipeStepMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link RecipeStep} entities in the database.
 * The main input is a {@link RecipeStepCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RecipeStepDTO} or a {@link Page} of {@link RecipeStepDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RecipeStepQueryService extends QueryService<RecipeStep> {

    private final Logger log = LoggerFactory.getLogger(RecipeStepQueryService.class);

    private final RecipeStepRepository recipeStepRepository;

    private final RecipeStepMapper recipeStepMapper;

    private final RecipeStepSearchRepository recipeStepSearchRepository;

    public RecipeStepQueryService(
        RecipeStepRepository recipeStepRepository,
        RecipeStepMapper recipeStepMapper,
        RecipeStepSearchRepository recipeStepSearchRepository
    ) {
        this.recipeStepRepository = recipeStepRepository;
        this.recipeStepMapper = recipeStepMapper;
        this.recipeStepSearchRepository = recipeStepSearchRepository;
    }

    /**
     * Return a {@link List} of {@link RecipeStepDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RecipeStepDTO> findByCriteria(RecipeStepCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RecipeStep> specification = createSpecification(criteria);
        return recipeStepMapper.toDto(recipeStepRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link RecipeStepDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RecipeStepDTO> findByCriteria(RecipeStepCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RecipeStep> specification = createSpecification(criteria);
        return recipeStepRepository.findAll(specification, page).map(recipeStepMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RecipeStepCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RecipeStep> specification = createSpecification(criteria);
        return recipeStepRepository.count(specification);
    }

    /**
     * Function to convert {@link RecipeStepCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RecipeStep> createSpecification(RecipeStepCriteria criteria) {
        Specification<RecipeStep> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), RecipeStep_.id));
            }
            if (criteria.getStepNum() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStepNum(), RecipeStep_.stepNum));
            }
            if (criteria.getStepDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStepDescription(), RecipeStep_.stepDescription));
            }
            if (criteria.getRecipeId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getRecipeId(), root -> root.join(RecipeStep_.recipes, JoinType.LEFT).get(Recipe_.id))
                    );
            }
        }
        return specification;
    }
}

package com.mgoulene.mymenu.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.*;

import com.mgoulene.mymenu.repository.RecipePictureRepository;
import com.mgoulene.mymenu.service.RecipePictureQueryService;
import com.mgoulene.mymenu.service.RecipePictureService;
import com.mgoulene.mymenu.service.criteria.RecipePictureCriteria;
import com.mgoulene.mymenu.service.dto.RecipePictureDTO;
import com.mgoulene.mymenu.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.StreamSupport;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mgoulene.mymenu.domain.RecipePicture}.
 */
@RestController
@RequestMapping("/api")
public class RecipePictureResource {

    private final Logger log = LoggerFactory.getLogger(RecipePictureResource.class);

    private static final String ENTITY_NAME = "recipePicture";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RecipePictureService recipePictureService;

    private final RecipePictureRepository recipePictureRepository;

    private final RecipePictureQueryService recipePictureQueryService;

    public RecipePictureResource(
        RecipePictureService recipePictureService,
        RecipePictureRepository recipePictureRepository,
        RecipePictureQueryService recipePictureQueryService
    ) {
        this.recipePictureService = recipePictureService;
        this.recipePictureRepository = recipePictureRepository;
        this.recipePictureQueryService = recipePictureQueryService;
    }

    /**
     * {@code POST  /recipe-pictures} : Create a new recipePicture.
     *
     * @param recipePictureDTO the recipePictureDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new recipePictureDTO, or with status {@code 400 (Bad Request)} if the recipePicture has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/recipe-pictures")
    public ResponseEntity<RecipePictureDTO> createRecipePicture(@Valid @RequestBody RecipePictureDTO recipePictureDTO)
        throws URISyntaxException {
        log.debug("REST request to save RecipePicture : {}", recipePictureDTO);
        if (recipePictureDTO.getId() != null) {
            throw new BadRequestAlertException("A new recipePicture cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RecipePictureDTO result = recipePictureService.save(recipePictureDTO);
        return ResponseEntity
            .created(new URI("/api/recipe-pictures/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /recipe-pictures/:id} : Updates an existing recipePicture.
     *
     * @param id the id of the recipePictureDTO to save.
     * @param recipePictureDTO the recipePictureDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated recipePictureDTO,
     * or with status {@code 400 (Bad Request)} if the recipePictureDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the recipePictureDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/recipe-pictures/{id}")
    public ResponseEntity<RecipePictureDTO> updateRecipePicture(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody RecipePictureDTO recipePictureDTO
    ) throws URISyntaxException {
        log.debug("REST request to update RecipePicture : {}, {}", id, recipePictureDTO);
        if (recipePictureDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, recipePictureDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!recipePictureRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        RecipePictureDTO result = recipePictureService.save(recipePictureDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, recipePictureDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /recipe-pictures/:id} : Partial updates given fields of an existing recipePicture, field will ignore if it is null
     *
     * @param id the id of the recipePictureDTO to save.
     * @param recipePictureDTO the recipePictureDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated recipePictureDTO,
     * or with status {@code 400 (Bad Request)} if the recipePictureDTO is not valid,
     * or with status {@code 404 (Not Found)} if the recipePictureDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the recipePictureDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/recipe-pictures/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<RecipePictureDTO> partialUpdateRecipePicture(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody RecipePictureDTO recipePictureDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update RecipePicture partially : {}, {}", id, recipePictureDTO);
        if (recipePictureDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, recipePictureDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!recipePictureRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<RecipePictureDTO> result = recipePictureService.partialUpdate(recipePictureDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, recipePictureDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /recipe-pictures} : get all the recipePictures.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of recipePictures in body.
     */
    @GetMapping("/recipe-pictures")
    public ResponseEntity<List<RecipePictureDTO>> getAllRecipePictures(
        RecipePictureCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get RecipePictures by criteria: {}", criteria);
        Page<RecipePictureDTO> page = recipePictureQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /recipe-pictures/count} : count all the recipePictures.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/recipe-pictures/count")
    public ResponseEntity<Long> countRecipePictures(RecipePictureCriteria criteria) {
        log.debug("REST request to count RecipePictures by criteria: {}", criteria);
        return ResponseEntity.ok().body(recipePictureQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /recipe-pictures/:id} : get the "id" recipePicture.
     *
     * @param id the id of the recipePictureDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the recipePictureDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/recipe-pictures/{id}")
    public ResponseEntity<RecipePictureDTO> getRecipePicture(@PathVariable Long id) {
        log.debug("REST request to get RecipePicture : {}", id);
        Optional<RecipePictureDTO> recipePictureDTO = recipePictureService.findOne(id);
        return ResponseUtil.wrapOrNotFound(recipePictureDTO);
    }

    /**
     * {@code DELETE  /recipe-pictures/:id} : delete the "id" recipePicture.
     *
     * @param id the id of the recipePictureDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/recipe-pictures/{id}")
    public ResponseEntity<Void> deleteRecipePicture(@PathVariable Long id) {
        log.debug("REST request to delete RecipePicture : {}", id);
        recipePictureService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code SEARCH  /_search/recipe-pictures?query=:query} : search for the recipePicture corresponding
     * to the query.
     *
     * @param query the query of the recipePicture search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/recipe-pictures")
    public ResponseEntity<List<RecipePictureDTO>> searchRecipePictures(
        @RequestParam String query,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to search for a page of RecipePictures for query {}", query);
        Page<RecipePictureDTO> page = recipePictureService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}

package com.mgoulene.mymenu.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.*;

import com.mgoulene.mymenu.repository.SheldRepository;
import com.mgoulene.mymenu.service.SheldQueryService;
import com.mgoulene.mymenu.service.SheldService;
import com.mgoulene.mymenu.service.criteria.SheldCriteria;
import com.mgoulene.mymenu.service.dto.SheldDTO;
import com.mgoulene.mymenu.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.StreamSupport;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mgoulene.mymenu.domain.Sheld}.
 */
@RestController
@RequestMapping("/api")
public class SheldResource {

    private final Logger log = LoggerFactory.getLogger(SheldResource.class);

    private static final String ENTITY_NAME = "sheld";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SheldService sheldService;

    private final SheldRepository sheldRepository;

    private final SheldQueryService sheldQueryService;

    public SheldResource(SheldService sheldService, SheldRepository sheldRepository, SheldQueryService sheldQueryService) {
        this.sheldService = sheldService;
        this.sheldRepository = sheldRepository;
        this.sheldQueryService = sheldQueryService;
    }

    /**
     * {@code POST  /shelds} : Create a new sheld.
     *
     * @param sheldDTO the sheldDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new sheldDTO, or with status {@code 400 (Bad Request)} if the sheld has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shelds")
    public ResponseEntity<SheldDTO> createSheld(@Valid @RequestBody SheldDTO sheldDTO) throws URISyntaxException {
        log.debug("REST request to save Sheld : {}", sheldDTO);
        if (sheldDTO.getId() != null) {
            throw new BadRequestAlertException("A new sheld cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SheldDTO result = sheldService.save(sheldDTO);
        return ResponseEntity
            .created(new URI("/api/shelds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shelds/:id} : Updates an existing sheld.
     *
     * @param id the id of the sheldDTO to save.
     * @param sheldDTO the sheldDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sheldDTO,
     * or with status {@code 400 (Bad Request)} if the sheldDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the sheldDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shelds/{id}")
    public ResponseEntity<SheldDTO> updateSheld(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody SheldDTO sheldDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Sheld : {}, {}", id, sheldDTO);
        if (sheldDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, sheldDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!sheldRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        SheldDTO result = sheldService.save(sheldDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, sheldDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /shelds/:id} : Partial updates given fields of an existing sheld, field will ignore if it is null
     *
     * @param id the id of the sheldDTO to save.
     * @param sheldDTO the sheldDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sheldDTO,
     * or with status {@code 400 (Bad Request)} if the sheldDTO is not valid,
     * or with status {@code 404 (Not Found)} if the sheldDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the sheldDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/shelds/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<SheldDTO> partialUpdateSheld(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody SheldDTO sheldDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Sheld partially : {}, {}", id, sheldDTO);
        if (sheldDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, sheldDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!sheldRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<SheldDTO> result = sheldService.partialUpdate(sheldDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, sheldDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /shelds} : get all the shelds.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shelds in body.
     */
    @GetMapping("/shelds")
    public ResponseEntity<List<SheldDTO>> getAllShelds(
        SheldCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get Shelds by criteria: {}", criteria);
        Page<SheldDTO> page = sheldQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /shelds/count} : count all the shelds.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/shelds/count")
    public ResponseEntity<Long> countShelds(SheldCriteria criteria) {
        log.debug("REST request to count Shelds by criteria: {}", criteria);
        return ResponseEntity.ok().body(sheldQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /shelds/:id} : get the "id" sheld.
     *
     * @param id the id of the sheldDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the sheldDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shelds/{id}")
    public ResponseEntity<SheldDTO> getSheld(@PathVariable Long id) {
        log.debug("REST request to get Sheld : {}", id);
        Optional<SheldDTO> sheldDTO = sheldService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sheldDTO);
    }

    /**
     * {@code DELETE  /shelds/:id} : delete the "id" sheld.
     *
     * @param id the id of the sheldDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shelds/{id}")
    public ResponseEntity<Void> deleteSheld(@PathVariable Long id) {
        log.debug("REST request to delete Sheld : {}", id);
        sheldService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code SEARCH  /_search/shelds?query=:query} : search for the sheld corresponding
     * to the query.
     *
     * @param query the query of the sheld search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/shelds")
    public ResponseEntity<List<SheldDTO>> searchShelds(
        @RequestParam String query,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to search for a page of Shelds for query {}", query);
        Page<SheldDTO> page = sheldService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}

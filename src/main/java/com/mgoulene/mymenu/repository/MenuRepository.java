package com.mgoulene.mymenu.repository;

import com.mgoulene.mymenu.domain.Menu;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Menu entity.
 */
@Repository
public interface MenuRepository extends JpaRepository<Menu, Long>, JpaSpecificationExecutor<Menu> {
    default Optional<Menu> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<Menu> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<Menu> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct menu from Menu menu left join fetch menu.starter left join fetch menu.main left join fetch menu.dessert",
        countQuery = "select count(distinct menu) from Menu menu"
    )
    Page<Menu> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct menu from Menu menu left join fetch menu.starter left join fetch menu.main left join fetch menu.dessert")
    List<Menu> findAllWithToOneRelationships();

    @Query(
        "select menu from Menu menu left join fetch menu.starter left join fetch menu.main left join fetch menu.dessert where menu.id =:id"
    )
    Optional<Menu> findOneWithToOneRelationships(@Param("id") Long id);
}

package com.mgoulene.mymenu.repository.search;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import com.mgoulene.mymenu.domain.RecipePicture;
import java.util.List;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.elasticsearch.search.sort.SortBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link RecipePicture} entity.
 */
public interface RecipePictureSearchRepository
    extends ElasticsearchRepository<RecipePicture, Long>, RecipePictureSearchRepositoryInternal {}

interface RecipePictureSearchRepositoryInternal {
    Page<RecipePicture> search(String query, Pageable pageable);
}

class RecipePictureSearchRepositoryInternalImpl implements RecipePictureSearchRepositoryInternal {

    private final ElasticsearchRestTemplate elasticsearchTemplate;

    RecipePictureSearchRepositoryInternalImpl(ElasticsearchRestTemplate elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    @Override
    public Page<RecipePicture> search(String query, Pageable pageable) {
        NativeSearchQuery nativeSearchQuery = new NativeSearchQuery(queryStringQuery(query));
        nativeSearchQuery.setPageable(pageable);
        List<RecipePicture> hits = elasticsearchTemplate
            .search(nativeSearchQuery, RecipePicture.class)
            .map(SearchHit::getContent)
            .stream()
            .collect(Collectors.toList());

        return new PageImpl<>(hits, pageable, hits.size());
    }
}

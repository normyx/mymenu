package com.mgoulene.mymenu.repository.search;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import com.mgoulene.mymenu.domain.Sheld;
import java.util.List;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.elasticsearch.search.sort.SortBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Sheld} entity.
 */
public interface SheldSearchRepository extends ElasticsearchRepository<Sheld, Long>, SheldSearchRepositoryInternal {}

interface SheldSearchRepositoryInternal {
    Page<Sheld> search(String query, Pageable pageable);
}

class SheldSearchRepositoryInternalImpl implements SheldSearchRepositoryInternal {

    private final ElasticsearchRestTemplate elasticsearchTemplate;

    SheldSearchRepositoryInternalImpl(ElasticsearchRestTemplate elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    @Override
    public Page<Sheld> search(String query, Pageable pageable) {
        NativeSearchQuery nativeSearchQuery = new NativeSearchQuery(queryStringQuery(query));
        nativeSearchQuery.setPageable(pageable);
        List<Sheld> hits = elasticsearchTemplate
            .search(nativeSearchQuery, Sheld.class)
            .map(SearchHit::getContent)
            .stream()
            .collect(Collectors.toList());

        return new PageImpl<>(hits, pageable, hits.size());
    }
}

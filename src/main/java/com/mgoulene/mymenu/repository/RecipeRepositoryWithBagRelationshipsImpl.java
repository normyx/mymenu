package com.mgoulene.mymenu.repository;

import com.mgoulene.mymenu.domain.Recipe;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.hibernate.annotations.QueryHints;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

/**
 * Utility repository to load bag relationships based on https://vladmihalcea.com/hibernate-multiplebagfetchexception/
 */
public class RecipeRepositoryWithBagRelationshipsImpl implements RecipeRepositoryWithBagRelationships {

    @Autowired
    private EntityManager entityManager;

    @Override
    public Optional<Recipe> fetchBagRelationships(Optional<Recipe> recipe) {
        return recipe.map(this::fetchTags).map(this::fetchIngredients).map(this::fetchRecipePictures).map(this::fetchRecipeSteps);
    }

    @Override
    public Page<Recipe> fetchBagRelationships(Page<Recipe> recipes) {
        return new PageImpl<>(fetchBagRelationships(recipes.getContent()), recipes.getPageable(), recipes.getTotalElements());
    }

    @Override
    public List<Recipe> fetchBagRelationships(List<Recipe> recipes) {
        return Optional
            .of(recipes)
            .map(this::fetchTags)
            .map(this::fetchIngredients)
            .map(this::fetchRecipePictures)
            .map(this::fetchRecipeSteps)
            .get();
    }

    Recipe fetchTags(Recipe result) {
        return entityManager
            .createQuery("select recipe from Recipe recipe left join fetch recipe.tags where recipe is :recipe", Recipe.class)
            .setParameter("recipe", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<Recipe> fetchTags(List<Recipe> recipes) {
        return entityManager
            .createQuery("select distinct recipe from Recipe recipe left join fetch recipe.tags where recipe in :recipes", Recipe.class)
            .setParameter("recipes", recipes)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
    }

    Recipe fetchIngredients(Recipe result) {
        return entityManager
            .createQuery("select recipe from Recipe recipe left join fetch recipe.ingredients where recipe is :recipe", Recipe.class)
            .setParameter("recipe", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<Recipe> fetchIngredients(List<Recipe> recipes) {
        return entityManager
            .createQuery(
                "select distinct recipe from Recipe recipe left join fetch recipe.ingredients where recipe in :recipes",
                Recipe.class
            )
            .setParameter("recipes", recipes)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
    }

    Recipe fetchRecipePictures(Recipe result) {
        return entityManager
            .createQuery("select recipe from Recipe recipe left join fetch recipe.recipePictures where recipe is :recipe", Recipe.class)
            .setParameter("recipe", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<Recipe> fetchRecipePictures(List<Recipe> recipes) {
        return entityManager
            .createQuery(
                "select distinct recipe from Recipe recipe left join fetch recipe.recipePictures where recipe in :recipes",
                Recipe.class
            )
            .setParameter("recipes", recipes)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
    }

    Recipe fetchRecipeSteps(Recipe result) {
        return entityManager
            .createQuery("select recipe from Recipe recipe left join fetch recipe.recipeSteps where recipe is :recipe", Recipe.class)
            .setParameter("recipe", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<Recipe> fetchRecipeSteps(List<Recipe> recipes) {
        return entityManager
            .createQuery(
                "select distinct recipe from Recipe recipe left join fetch recipe.recipeSteps where recipe in :recipes",
                Recipe.class
            )
            .setParameter("recipes", recipes)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
    }
}

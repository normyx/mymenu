package com.mgoulene.mymenu.repository;

import com.mgoulene.mymenu.domain.RecipePicture;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the RecipePicture entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RecipePictureRepository extends JpaRepository<RecipePicture, Long>, JpaSpecificationExecutor<RecipePicture> {}

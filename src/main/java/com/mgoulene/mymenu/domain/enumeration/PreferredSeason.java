package com.mgoulene.mymenu.domain.enumeration;

/**
 * The PreferredSeason enumeration.
 */
public enum PreferredSeason {
    TRUE,
    FALSE,
}

package com.mgoulene.mymenu.domain.enumeration;

/**
 * The QuantityType enumeration.
 */
public enum QuantityType {
    QUANTITY,
    G,
    KG,
    L,
    ML,
    DC,
    SOUPSPOON,
    TABLESPOON,
    TEASPON,
    CUP,
    PINCH,
}

package com.mgoulene.mymenu.domain.enumeration;

/**
 * The RecipeType enumeration.
 */
public enum RecipeType {
    APERITIF,
    STARTER,
    MAIN,
    DESSERT,
}

package com.mgoulene.mymenu.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mgoulene.mymenu.domain.enumeration.PreferredSeason;
import com.mgoulene.mymenu.domain.enumeration.RecipeType;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * The Recipe entity.\n@author A true hipster
 */
@Entity
@Table(name = "recipe")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "recipe")
public class Recipe implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    /**
     * fieldName
     */
    @NotNull
    @Size(max = 400)
    @Column(name = "name", length = 400, nullable = false)
    private String name;

    @NotNull
    @Column(name = "creation_date", nullable = false)
    private LocalDate creationDate;

    @Size(max = 10000)
    @Column(name = "description", length = 10000)
    private String description;

    @Min(value = 0)
    @Column(name = "preparation_duration")
    private Integer preparationDuration;

    @Min(value = 0)
    @Column(name = "rest_duraction")
    private Integer restDuraction;

    @Min(value = 0)
    @Column(name = "cooking_duration")
    private Integer cookingDuration;

    @Enumerated(EnumType.STRING)
    @Column(name = "winter_preferrency")
    private PreferredSeason winterPreferrency;

    @Enumerated(EnumType.STRING)
    @Column(name = "spring_preferrency")
    private PreferredSeason springPreferrency;

    @Enumerated(EnumType.STRING)
    @Column(name = "summer_preferrency")
    private PreferredSeason summerPreferrency;

    @Enumerated(EnumType.STRING)
    @Column(name = "autumn_preferrency")
    private PreferredSeason autumnPreferrency;

    @Min(value = 1)
    @Column(name = "number_of_people")
    private Integer numberOfPeople;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "recipe_type", nullable = false)
    private RecipeType recipeType;

    @Size(max = 400)
    @Column(name = "recipe_url", length = 400)
    private String recipeURL;

    @Size(max = 40000)
    @Column(name = "short_steps", length = 40000)
    private String shortSteps;

    @NotNull
    @Column(name = "is_short_desc_recipe", nullable = false)
    private Boolean isShortDescRecipe;

    @ManyToMany
    @JoinTable(name = "rel_recipe__tag", joinColumns = @JoinColumn(name = "recipe_id"), inverseJoinColumns = @JoinColumn(name = "tag_id"))
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "recipes" }, allowSetters = true)
    private Set<Tag> tags = new HashSet<>();

    @ManyToMany
    @JoinTable(
        name = "rel_recipe__ingredient",
        joinColumns = @JoinColumn(name = "recipe_id"),
        inverseJoinColumns = @JoinColumn(name = "ingredient_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "product", "recipes" }, allowSetters = true)
    private Set<Ingredient> ingredients = new HashSet<>();

    @ManyToMany
    @JoinTable(
        name = "rel_recipe__recipe_picture",
        joinColumns = @JoinColumn(name = "recipe_id"),
        inverseJoinColumns = @JoinColumn(name = "recipe_picture_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "recipes" }, allowSetters = true)
    private Set<RecipePicture> recipePictures = new HashSet<>();

    @ManyToMany
    @JoinTable(
        name = "rel_recipe__recipe_step",
        joinColumns = @JoinColumn(name = "recipe_id"),
        inverseJoinColumns = @JoinColumn(name = "recipe_step_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "recipes" }, allowSetters = true)
    private Set<RecipeStep> recipeSteps = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Recipe id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Recipe name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getCreationDate() {
        return this.creationDate;
    }

    public Recipe creationDate(LocalDate creationDate) {
        this.setCreationDate(creationDate);
        return this;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public String getDescription() {
        return this.description;
    }

    public Recipe description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPreparationDuration() {
        return this.preparationDuration;
    }

    public Recipe preparationDuration(Integer preparationDuration) {
        this.setPreparationDuration(preparationDuration);
        return this;
    }

    public void setPreparationDuration(Integer preparationDuration) {
        this.preparationDuration = preparationDuration;
    }

    public Integer getRestDuraction() {
        return this.restDuraction;
    }

    public Recipe restDuraction(Integer restDuraction) {
        this.setRestDuraction(restDuraction);
        return this;
    }

    public void setRestDuraction(Integer restDuraction) {
        this.restDuraction = restDuraction;
    }

    public Integer getCookingDuration() {
        return this.cookingDuration;
    }

    public Recipe cookingDuration(Integer cookingDuration) {
        this.setCookingDuration(cookingDuration);
        return this;
    }

    public void setCookingDuration(Integer cookingDuration) {
        this.cookingDuration = cookingDuration;
    }

    public PreferredSeason getWinterPreferrency() {
        return this.winterPreferrency;
    }

    public Recipe winterPreferrency(PreferredSeason winterPreferrency) {
        this.setWinterPreferrency(winterPreferrency);
        return this;
    }

    public void setWinterPreferrency(PreferredSeason winterPreferrency) {
        this.winterPreferrency = winterPreferrency;
    }

    public PreferredSeason getSpringPreferrency() {
        return this.springPreferrency;
    }

    public Recipe springPreferrency(PreferredSeason springPreferrency) {
        this.setSpringPreferrency(springPreferrency);
        return this;
    }

    public void setSpringPreferrency(PreferredSeason springPreferrency) {
        this.springPreferrency = springPreferrency;
    }

    public PreferredSeason getSummerPreferrency() {
        return this.summerPreferrency;
    }

    public Recipe summerPreferrency(PreferredSeason summerPreferrency) {
        this.setSummerPreferrency(summerPreferrency);
        return this;
    }

    public void setSummerPreferrency(PreferredSeason summerPreferrency) {
        this.summerPreferrency = summerPreferrency;
    }

    public PreferredSeason getAutumnPreferrency() {
        return this.autumnPreferrency;
    }

    public Recipe autumnPreferrency(PreferredSeason autumnPreferrency) {
        this.setAutumnPreferrency(autumnPreferrency);
        return this;
    }

    public void setAutumnPreferrency(PreferredSeason autumnPreferrency) {
        this.autumnPreferrency = autumnPreferrency;
    }

    public Integer getNumberOfPeople() {
        return this.numberOfPeople;
    }

    public Recipe numberOfPeople(Integer numberOfPeople) {
        this.setNumberOfPeople(numberOfPeople);
        return this;
    }

    public void setNumberOfPeople(Integer numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public RecipeType getRecipeType() {
        return this.recipeType;
    }

    public Recipe recipeType(RecipeType recipeType) {
        this.setRecipeType(recipeType);
        return this;
    }

    public void setRecipeType(RecipeType recipeType) {
        this.recipeType = recipeType;
    }

    public String getRecipeURL() {
        return this.recipeURL;
    }

    public Recipe recipeURL(String recipeURL) {
        this.setRecipeURL(recipeURL);
        return this;
    }

    public void setRecipeURL(String recipeURL) {
        this.recipeURL = recipeURL;
    }

    public String getShortSteps() {
        return this.shortSteps;
    }

    public Recipe shortSteps(String shortSteps) {
        this.setShortSteps(shortSteps);
        return this;
    }

    public void setShortSteps(String shortSteps) {
        this.shortSteps = shortSteps;
    }

    public Boolean getIsShortDescRecipe() {
        return this.isShortDescRecipe;
    }

    public Recipe isShortDescRecipe(Boolean isShortDescRecipe) {
        this.setIsShortDescRecipe(isShortDescRecipe);
        return this;
    }

    public void setIsShortDescRecipe(Boolean isShortDescRecipe) {
        this.isShortDescRecipe = isShortDescRecipe;
    }

    public Set<Tag> getTags() {
        return this.tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Recipe tags(Set<Tag> tags) {
        this.setTags(tags);
        return this;
    }

    public Recipe addTag(Tag tag) {
        this.tags.add(tag);
        tag.getRecipes().add(this);
        return this;
    }

    public Recipe removeTag(Tag tag) {
        this.tags.remove(tag);
        tag.getRecipes().remove(this);
        return this;
    }

    public Set<Ingredient> getIngredients() {
        return this.ingredients;
    }

    public void setIngredients(Set<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public Recipe ingredients(Set<Ingredient> ingredients) {
        this.setIngredients(ingredients);
        return this;
    }

    public Recipe addIngredient(Ingredient ingredient) {
        this.ingredients.add(ingredient);
        ingredient.getRecipes().add(this);
        return this;
    }

    public Recipe removeIngredient(Ingredient ingredient) {
        this.ingredients.remove(ingredient);
        ingredient.getRecipes().remove(this);
        return this;
    }

    public Set<RecipePicture> getRecipePictures() {
        return this.recipePictures;
    }

    public void setRecipePictures(Set<RecipePicture> recipePictures) {
        this.recipePictures = recipePictures;
    }

    public Recipe recipePictures(Set<RecipePicture> recipePictures) {
        this.setRecipePictures(recipePictures);
        return this;
    }

    public Recipe addRecipePicture(RecipePicture recipePicture) {
        this.recipePictures.add(recipePicture);
        recipePicture.getRecipes().add(this);
        return this;
    }

    public Recipe removeRecipePicture(RecipePicture recipePicture) {
        this.recipePictures.remove(recipePicture);
        recipePicture.getRecipes().remove(this);
        return this;
    }

    public Set<RecipeStep> getRecipeSteps() {
        return this.recipeSteps;
    }

    public void setRecipeSteps(Set<RecipeStep> recipeSteps) {
        this.recipeSteps = recipeSteps;
    }

    public Recipe recipeSteps(Set<RecipeStep> recipeSteps) {
        this.setRecipeSteps(recipeSteps);
        return this;
    }

    public Recipe addRecipeStep(RecipeStep recipeStep) {
        this.recipeSteps.add(recipeStep);
        recipeStep.getRecipes().add(this);
        return this;
    }

    public Recipe removeRecipeStep(RecipeStep recipeStep) {
        this.recipeSteps.remove(recipeStep);
        recipeStep.getRecipes().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Recipe)) {
            return false;
        }
        return id != null && id.equals(((Recipe) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Recipe{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", description='" + getDescription() + "'" +
            ", preparationDuration=" + getPreparationDuration() +
            ", restDuraction=" + getRestDuraction() +
            ", cookingDuration=" + getCookingDuration() +
            ", winterPreferrency='" + getWinterPreferrency() + "'" +
            ", springPreferrency='" + getSpringPreferrency() + "'" +
            ", summerPreferrency='" + getSummerPreferrency() + "'" +
            ", autumnPreferrency='" + getAutumnPreferrency() + "'" +
            ", numberOfPeople=" + getNumberOfPeople() +
            ", recipeType='" + getRecipeType() + "'" +
            ", recipeURL='" + getRecipeURL() + "'" +
            ", shortSteps='" + getShortSteps() + "'" +
            ", isShortDescRecipe='" + getIsShortDescRecipe() + "'" +
            "}";
    }
}

import { NgModule } from '@angular/core';
import { MMRecipeDetailComponent } from './detail/mmrecipe-detail.component';
import { SharedModule } from 'app/shared/shared.module';
import { MMRecipeRoutingModule } from './route/recipe-routing.module';

@NgModule({
  declarations: [MMRecipeDetailComponent],
  imports: [SharedModule, MMRecipeRoutingModule],
})
export class MMRecipeModule {}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IRecipe } from 'app/entities/recipe/recipe.model';

@Component({
  selector: 'jhi-mmrecipe-detail',
  templateUrl: './mmrecipe-detail.component.html',
})
export class MMRecipeDetailComponent implements OnInit {
  recipe: IRecipe | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ recipe }) => {
      this.recipe = recipe;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

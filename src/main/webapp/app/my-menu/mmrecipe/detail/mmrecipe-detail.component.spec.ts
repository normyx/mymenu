import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MMRecipeDetailComponent } from './mmrecipe-detail.component';

describe('MMRecipeDetailComponent', () => {
  let component: MMRecipeDetailComponent;
  let fixture: ComponentFixture<MMRecipeDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MMRecipeDetailComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MMRecipeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

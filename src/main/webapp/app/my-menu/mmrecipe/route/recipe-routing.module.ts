import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { RecipeRoutingResolveService } from 'app/entities/recipe/route/recipe-routing-resolve.service';
import { MMRecipeDetailComponent } from '../detail/mmrecipe-detail.component';

const recipeRoute: Routes = [
  {
    path: ':id/view',
    component: MMRecipeDetailComponent,
    resolve: {
      recipe: RecipeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(recipeRoute)],
  exports: [RouterModule],
})
export class MMRecipeRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IIngredient } from 'app/entities/ingredient/ingredient.model';

@Component({
  selector: 'jhi-mmingredient-detail',
  templateUrl: './mmingredient-detail.component.html',
})
export class MMIngredientDetailComponent implements OnInit {
  ingredient: IIngredient | null = null;
  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ ingredient }) => {
      this.ingredient = ingredient;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MMIngredientDetailComponent } from './mmingredient-detail.component';

describe('MMIngredientDetailComponent', () => {
  let component: MMIngredientDetailComponent;
  let fixture: ComponentFixture<MMIngredientDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MMIngredientDetailComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MMIngredientDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { IngredientRoutingResolveService } from 'app/entities/ingredient/route/ingredient-routing-resolve.service';
import { MMIngredientDetailComponent } from '../detail/mmingredient-detail.component';

const ingredientRoute: Routes = [
  {
    path: ':id/view',
    component: MMIngredientDetailComponent,
    resolve: {
      ingredient: IngredientRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(ingredientRoute)],
  exports: [RouterModule],
})
export class MMIngredientRoutingModule {}

import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { MMIngredientRoutingModule } from './route/mmingredient-routing.module';
import { MMIngredientDetailComponent } from './detail/mmingredient-detail.component';

@NgModule({
  imports: [SharedModule, MMIngredientRoutingModule],
  declarations: [MMIngredientDetailComponent],
})
export class MMIngredientModule {}

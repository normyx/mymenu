import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'mmingredient',
        data: { pageTitle: 'mymenuApp.ingredient.home.title' },
        loadChildren: () => import('./mmingredient/mmingredient.module').then(m => m.MMIngredientModule),
      },
      {
        path: 'mmrecipe',
        data: { pageTitle: 'mymenuApp.ingredient.home.title' },
        loadChildren: () => import('./mmrecipe/mmrecipe.module').then(m => m.MMRecipeModule),
      },

      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class MyMenuRoutingModule {}

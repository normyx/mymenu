import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SheldDetailComponent } from './sheld-detail.component';

describe('Sheld Management Detail Component', () => {
  let comp: SheldDetailComponent;
  let fixture: ComponentFixture<SheldDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SheldDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ sheld: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(SheldDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(SheldDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load sheld on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.sheld).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISheld } from '../sheld.model';

@Component({
  selector: 'jhi-sheld-detail',
  templateUrl: './sheld-detail.component.html',
})
export class SheldDetailComponent implements OnInit {
  sheld: ISheld | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ sheld }) => {
      this.sheld = sheld;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

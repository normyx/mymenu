import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ISheld } from '../sheld.model';
import { SheldService } from '../service/sheld.service';

@Component({
  templateUrl: './sheld-delete-dialog.component.html',
})
export class SheldDeleteDialogComponent {
  sheld?: ISheld;

  constructor(protected sheldService: SheldService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.sheldService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}

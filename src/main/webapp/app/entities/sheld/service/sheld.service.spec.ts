import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ISheld, Sheld } from '../sheld.model';

import { SheldService } from './sheld.service';

describe('Sheld Service', () => {
  let service: SheldService;
  let httpMock: HttpTestingController;
  let elemDefault: ISheld;
  let expectedResult: ISheld | ISheld[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(SheldService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Sheld', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Sheld()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Sheld', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Sheld', () => {
      const patchObject = Object.assign(
        {
          name: 'BBBBBB',
        },
        new Sheld()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Sheld', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Sheld', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addSheldToCollectionIfMissing', () => {
      it('should add a Sheld to an empty array', () => {
        const sheld: ISheld = { id: 123 };
        expectedResult = service.addSheldToCollectionIfMissing([], sheld);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(sheld);
      });

      it('should not add a Sheld to an array that contains it', () => {
        const sheld: ISheld = { id: 123 };
        const sheldCollection: ISheld[] = [
          {
            ...sheld,
          },
          { id: 456 },
        ];
        expectedResult = service.addSheldToCollectionIfMissing(sheldCollection, sheld);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Sheld to an array that doesn't contain it", () => {
        const sheld: ISheld = { id: 123 };
        const sheldCollection: ISheld[] = [{ id: 456 }];
        expectedResult = service.addSheldToCollectionIfMissing(sheldCollection, sheld);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(sheld);
      });

      it('should add only unique Sheld to an array', () => {
        const sheldArray: ISheld[] = [{ id: 123 }, { id: 456 }, { id: 77869 }];
        const sheldCollection: ISheld[] = [{ id: 123 }];
        expectedResult = service.addSheldToCollectionIfMissing(sheldCollection, ...sheldArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const sheld: ISheld = { id: 123 };
        const sheld2: ISheld = { id: 456 };
        expectedResult = service.addSheldToCollectionIfMissing([], sheld, sheld2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(sheld);
        expect(expectedResult).toContain(sheld2);
      });

      it('should accept null and undefined values', () => {
        const sheld: ISheld = { id: 123 };
        expectedResult = service.addSheldToCollectionIfMissing([], null, sheld, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(sheld);
      });

      it('should return initial array if no Sheld is added', () => {
        const sheldCollection: ISheld[] = [{ id: 123 }];
        expectedResult = service.addSheldToCollectionIfMissing(sheldCollection, undefined, null);
        expect(expectedResult).toEqual(sheldCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'menu',
        data: { pageTitle: 'mymenuApp.menu.home.title' },
        loadChildren: () => import('./menu/menu.module').then(m => m.MenuModule),
      },
      {
        path: 'recipe',
        data: { pageTitle: 'mymenuApp.recipe.home.title' },
        loadChildren: () => import('./recipe/recipe.module').then(m => m.RecipeModule),
      },
      {
        path: 'tag',
        data: { pageTitle: 'mymenuApp.tag.home.title' },
        loadChildren: () => import('./tag/tag.module').then(m => m.TagModule),
      },
      {
        path: 'recipe-step',
        data: { pageTitle: 'mymenuApp.recipeStep.home.title' },
        loadChildren: () => import('./recipe-step/recipe-step.module').then(m => m.RecipeStepModule),
      },
      {
        path: 'recipe-picture',
        data: { pageTitle: 'mymenuApp.recipePicture.home.title' },
        loadChildren: () => import('./recipe-picture/recipe-picture.module').then(m => m.RecipePictureModule),
      },
      {
        path: 'ingredient',
        data: { pageTitle: 'mymenuApp.ingredient.home.title' },
        loadChildren: () => import('./ingredient/ingredient.module').then(m => m.IngredientModule),
      },
      {
        path: 'sheld',
        data: { pageTitle: 'mymenuApp.sheld.home.title' },
        loadChildren: () => import('./sheld/sheld.module').then(m => m.SheldModule),
      },
      {
        path: 'product',
        data: { pageTitle: 'mymenuApp.product.home.title' },
        loadChildren: () => import('./product/product.module').then(m => m.ProductModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}

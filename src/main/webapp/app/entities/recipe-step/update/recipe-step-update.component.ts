import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IRecipeStep, RecipeStep } from '../recipe-step.model';
import { RecipeStepService } from '../service/recipe-step.service';

@Component({
  selector: 'jhi-recipe-step-update',
  templateUrl: './recipe-step-update.component.html',
})
export class RecipeStepUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    stepNum: [null, [Validators.required, Validators.min(1)]],
    stepDescription: [null, [Validators.maxLength(4000)]],
  });

  constructor(protected recipeStepService: RecipeStepService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ recipeStep }) => {
      this.updateForm(recipeStep);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const recipeStep = this.createFromForm();
    if (recipeStep.id !== undefined) {
      this.subscribeToSaveResponse(this.recipeStepService.update(recipeStep));
    } else {
      this.subscribeToSaveResponse(this.recipeStepService.create(recipeStep));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRecipeStep>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(recipeStep: IRecipeStep): void {
    this.editForm.patchValue({
      id: recipeStep.id,
      stepNum: recipeStep.stepNum,
      stepDescription: recipeStep.stepDescription,
    });
  }

  protected createFromForm(): IRecipeStep {
    return {
      ...new RecipeStep(),
      id: this.editForm.get(['id'])!.value,
      stepNum: this.editForm.get(['stepNum'])!.value,
      stepDescription: this.editForm.get(['stepDescription'])!.value,
    };
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { RecipeStepService } from '../service/recipe-step.service';
import { IRecipeStep, RecipeStep } from '../recipe-step.model';

import { RecipeStepUpdateComponent } from './recipe-step-update.component';

describe('RecipeStep Management Update Component', () => {
  let comp: RecipeStepUpdateComponent;
  let fixture: ComponentFixture<RecipeStepUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let recipeStepService: RecipeStepService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [RecipeStepUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(RecipeStepUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(RecipeStepUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    recipeStepService = TestBed.inject(RecipeStepService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const recipeStep: IRecipeStep = { id: 456 };

      activatedRoute.data = of({ recipeStep });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(recipeStep));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<RecipeStep>>();
      const recipeStep = { id: 123 };
      jest.spyOn(recipeStepService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ recipeStep });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: recipeStep }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(recipeStepService.update).toHaveBeenCalledWith(recipeStep);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<RecipeStep>>();
      const recipeStep = new RecipeStep();
      jest.spyOn(recipeStepService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ recipeStep });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: recipeStep }));
      saveSubject.complete();

      // THEN
      expect(recipeStepService.create).toHaveBeenCalledWith(recipeStep);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<RecipeStep>>();
      const recipeStep = { id: 123 };
      jest.spyOn(recipeStepService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ recipeStep });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(recipeStepService.update).toHaveBeenCalledWith(recipeStep);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});

import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IRecipeStep } from '../recipe-step.model';
import { RecipeStepService } from '../service/recipe-step.service';

@Component({
  templateUrl: './recipe-step-delete-dialog.component.html',
})
export class RecipeStepDeleteDialogComponent {
  recipeStep?: IRecipeStep;

  constructor(protected recipeStepService: RecipeStepService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.recipeStepService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}

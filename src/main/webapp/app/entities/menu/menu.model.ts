import dayjs from 'dayjs/esm';
import { IRecipe } from 'app/entities/recipe/recipe.model';

export interface IMenu {
  id?: number;
  date?: dayjs.Dayjs;
  userId?: string;
  starter?: IRecipe | null;
  main?: IRecipe | null;
  dessert?: IRecipe | null;
}

export class Menu implements IMenu {
  constructor(
    public id?: number,
    public date?: dayjs.Dayjs,
    public userId?: string,
    public starter?: IRecipe | null,
    public main?: IRecipe | null,
    public dessert?: IRecipe | null
  ) {}
}

export function getMenuIdentifier(menu: IMenu): number | undefined {
  return menu.id;
}

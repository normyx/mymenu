import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { RecipePictureService } from '../service/recipe-picture.service';
import { IRecipePicture, RecipePicture } from '../recipe-picture.model';

import { RecipePictureUpdateComponent } from './recipe-picture-update.component';

describe('RecipePicture Management Update Component', () => {
  let comp: RecipePictureUpdateComponent;
  let fixture: ComponentFixture<RecipePictureUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let recipePictureService: RecipePictureService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [RecipePictureUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(RecipePictureUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(RecipePictureUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    recipePictureService = TestBed.inject(RecipePictureService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const recipePicture: IRecipePicture = { id: 456 };

      activatedRoute.data = of({ recipePicture });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(recipePicture));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<RecipePicture>>();
      const recipePicture = { id: 123 };
      jest.spyOn(recipePictureService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ recipePicture });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: recipePicture }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(recipePictureService.update).toHaveBeenCalledWith(recipePicture);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<RecipePicture>>();
      const recipePicture = new RecipePicture();
      jest.spyOn(recipePictureService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ recipePicture });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: recipePicture }));
      saveSubject.complete();

      // THEN
      expect(recipePictureService.create).toHaveBeenCalledWith(recipePicture);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<RecipePicture>>();
      const recipePicture = { id: 123 };
      jest.spyOn(recipePictureService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ recipePicture });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(recipePictureService.update).toHaveBeenCalledWith(recipePicture);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});

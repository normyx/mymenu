import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { RecipePictureComponent } from './list/recipe-picture.component';
import { RecipePictureDetailComponent } from './detail/recipe-picture-detail.component';
import { RecipePictureUpdateComponent } from './update/recipe-picture-update.component';
import { RecipePictureDeleteDialogComponent } from './delete/recipe-picture-delete-dialog.component';
import { RecipePictureRoutingModule } from './route/recipe-picture-routing.module';

@NgModule({
  imports: [SharedModule, RecipePictureRoutingModule],
  declarations: [RecipePictureComponent, RecipePictureDetailComponent, RecipePictureUpdateComponent, RecipePictureDeleteDialogComponent],
  entryComponents: [RecipePictureDeleteDialogComponent],
})
export class RecipePictureModule {}

import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IRecipePicture } from '../recipe-picture.model';
import { RecipePictureService } from '../service/recipe-picture.service';

@Component({
  templateUrl: './recipe-picture-delete-dialog.component.html',
})
export class RecipePictureDeleteDialogComponent {
  recipePicture?: IRecipePicture;

  constructor(protected recipePictureService: RecipePictureService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.recipePictureService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { IRecipePicture, RecipePicture } from '../recipe-picture.model';
import { RecipePictureService } from '../service/recipe-picture.service';

import { RecipePictureRoutingResolveService } from './recipe-picture-routing-resolve.service';

describe('RecipePicture routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: RecipePictureRoutingResolveService;
  let service: RecipePictureService;
  let resultRecipePicture: IRecipePicture | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(RecipePictureRoutingResolveService);
    service = TestBed.inject(RecipePictureService);
    resultRecipePicture = undefined;
  });

  describe('resolve', () => {
    it('should return IRecipePicture returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultRecipePicture = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultRecipePicture).toEqual({ id: 123 });
    });

    it('should return new IRecipePicture if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultRecipePicture = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultRecipePicture).toEqual(new RecipePicture());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as RecipePicture })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultRecipePicture = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultRecipePicture).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});

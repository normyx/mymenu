import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IRecipePicture, RecipePicture } from '../recipe-picture.model';
import { RecipePictureService } from '../service/recipe-picture.service';

@Injectable({ providedIn: 'root' })
export class RecipePictureRoutingResolveService implements Resolve<IRecipePicture> {
  constructor(protected service: RecipePictureService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IRecipePicture> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((recipePicture: HttpResponse<RecipePicture>) => {
          if (recipePicture.body) {
            return of(recipePicture.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new RecipePicture());
  }
}

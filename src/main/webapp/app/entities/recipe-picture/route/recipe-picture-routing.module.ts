import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { RecipePictureComponent } from '../list/recipe-picture.component';
import { RecipePictureDetailComponent } from '../detail/recipe-picture-detail.component';
import { RecipePictureUpdateComponent } from '../update/recipe-picture-update.component';
import { RecipePictureRoutingResolveService } from './recipe-picture-routing-resolve.service';

const recipePictureRoute: Routes = [
  {
    path: '',
    component: RecipePictureComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: RecipePictureDetailComponent,
    resolve: {
      recipePicture: RecipePictureRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: RecipePictureUpdateComponent,
    resolve: {
      recipePicture: RecipePictureRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: RecipePictureUpdateComponent,
    resolve: {
      recipePicture: RecipePictureRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(recipePictureRoute)],
  exports: [RouterModule],
})
export class RecipePictureRoutingModule {}

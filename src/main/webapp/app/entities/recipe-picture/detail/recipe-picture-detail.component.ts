import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRecipePicture } from '../recipe-picture.model';
import { DataUtils } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-recipe-picture-detail',
  templateUrl: './recipe-picture-detail.component.html',
})
export class RecipePictureDetailComponent implements OnInit {
  recipePicture: IRecipePicture | null = null;

  constructor(protected dataUtils: DataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ recipePicture }) => {
      this.recipePicture = recipePicture;
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  previousState(): void {
    window.history.back();
  }
}

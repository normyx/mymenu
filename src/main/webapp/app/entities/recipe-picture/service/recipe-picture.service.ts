import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { SearchWithPagination } from 'app/core/request/request.model';
import { IRecipePicture, getRecipePictureIdentifier } from '../recipe-picture.model';

export type EntityResponseType = HttpResponse<IRecipePicture>;
export type EntityArrayResponseType = HttpResponse<IRecipePicture[]>;

@Injectable({ providedIn: 'root' })
export class RecipePictureService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/recipe-pictures');
  protected resourceSearchUrl = this.applicationConfigService.getEndpointFor('api/_search/recipe-pictures');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(recipePicture: IRecipePicture): Observable<EntityResponseType> {
    return this.http.post<IRecipePicture>(this.resourceUrl, recipePicture, { observe: 'response' });
  }

  update(recipePicture: IRecipePicture): Observable<EntityResponseType> {
    return this.http.put<IRecipePicture>(`${this.resourceUrl}/${getRecipePictureIdentifier(recipePicture) as number}`, recipePicture, {
      observe: 'response',
    });
  }

  partialUpdate(recipePicture: IRecipePicture): Observable<EntityResponseType> {
    return this.http.patch<IRecipePicture>(`${this.resourceUrl}/${getRecipePictureIdentifier(recipePicture) as number}`, recipePicture, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRecipePicture>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRecipePicture[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: SearchWithPagination): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRecipePicture[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }

  addRecipePictureToCollectionIfMissing(
    recipePictureCollection: IRecipePicture[],
    ...recipePicturesToCheck: (IRecipePicture | null | undefined)[]
  ): IRecipePicture[] {
    const recipePictures: IRecipePicture[] = recipePicturesToCheck.filter(isPresent);
    if (recipePictures.length > 0) {
      const recipePictureCollectionIdentifiers = recipePictureCollection.map(
        recipePictureItem => getRecipePictureIdentifier(recipePictureItem)!
      );
      const recipePicturesToAdd = recipePictures.filter(recipePictureItem => {
        const recipePictureIdentifier = getRecipePictureIdentifier(recipePictureItem);
        if (recipePictureIdentifier == null || recipePictureCollectionIdentifiers.includes(recipePictureIdentifier)) {
          return false;
        }
        recipePictureCollectionIdentifiers.push(recipePictureIdentifier);
        return true;
      });
      return [...recipePicturesToAdd, ...recipePictureCollection];
    }
    return recipePictureCollection;
  }
}

import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IRecipePicture, RecipePicture } from '../recipe-picture.model';

import { RecipePictureService } from './recipe-picture.service';

describe('RecipePicture Service', () => {
  let service: RecipePictureService;
  let httpMock: HttpTestingController;
  let elemDefault: IRecipePicture;
  let expectedResult: IRecipePicture | IRecipePicture[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(RecipePictureService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      pictureContentType: 'image/png',
      picture: 'AAAAAAA',
      label: 'AAAAAAA',
      order: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a RecipePicture', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new RecipePicture()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a RecipePicture', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          picture: 'BBBBBB',
          label: 'BBBBBB',
          order: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a RecipePicture', () => {
      const patchObject = Object.assign({}, new RecipePicture());

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of RecipePicture', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          picture: 'BBBBBB',
          label: 'BBBBBB',
          order: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a RecipePicture', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addRecipePictureToCollectionIfMissing', () => {
      it('should add a RecipePicture to an empty array', () => {
        const recipePicture: IRecipePicture = { id: 123 };
        expectedResult = service.addRecipePictureToCollectionIfMissing([], recipePicture);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(recipePicture);
      });

      it('should not add a RecipePicture to an array that contains it', () => {
        const recipePicture: IRecipePicture = { id: 123 };
        const recipePictureCollection: IRecipePicture[] = [
          {
            ...recipePicture,
          },
          { id: 456 },
        ];
        expectedResult = service.addRecipePictureToCollectionIfMissing(recipePictureCollection, recipePicture);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a RecipePicture to an array that doesn't contain it", () => {
        const recipePicture: IRecipePicture = { id: 123 };
        const recipePictureCollection: IRecipePicture[] = [{ id: 456 }];
        expectedResult = service.addRecipePictureToCollectionIfMissing(recipePictureCollection, recipePicture);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(recipePicture);
      });

      it('should add only unique RecipePicture to an array', () => {
        const recipePictureArray: IRecipePicture[] = [{ id: 123 }, { id: 456 }, { id: 26038 }];
        const recipePictureCollection: IRecipePicture[] = [{ id: 123 }];
        expectedResult = service.addRecipePictureToCollectionIfMissing(recipePictureCollection, ...recipePictureArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const recipePicture: IRecipePicture = { id: 123 };
        const recipePicture2: IRecipePicture = { id: 456 };
        expectedResult = service.addRecipePictureToCollectionIfMissing([], recipePicture, recipePicture2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(recipePicture);
        expect(expectedResult).toContain(recipePicture2);
      });

      it('should accept null and undefined values', () => {
        const recipePicture: IRecipePicture = { id: 123 };
        expectedResult = service.addRecipePictureToCollectionIfMissing([], null, recipePicture, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(recipePicture);
      });

      it('should return initial array if no RecipePicture is added', () => {
        const recipePictureCollection: IRecipePicture[] = [{ id: 123 }];
        expectedResult = service.addRecipePictureToCollectionIfMissing(recipePictureCollection, undefined, null);
        expect(expectedResult).toEqual(recipePictureCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

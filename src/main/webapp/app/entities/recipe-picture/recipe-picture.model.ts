import { IRecipe } from 'app/entities/recipe/recipe.model';

export interface IRecipePicture {
  id?: number;
  pictureContentType?: string;
  picture?: string;
  label?: string | null;
  order?: number;
  recipes?: IRecipe[] | null;
}

export class RecipePicture implements IRecipePicture {
  constructor(
    public id?: number,
    public pictureContentType?: string,
    public picture?: string,
    public label?: string | null,
    public order?: number,
    public recipes?: IRecipe[] | null
  ) {}
}

export function getRecipePictureIdentifier(recipePicture: IRecipePicture): number | undefined {
  return recipePicture.id;
}

export enum RecipeType {
  APERITIF = 'APERITIF',

  STARTER = 'STARTER',

  MAIN = 'MAIN',

  DESSERT = 'DESSERT',
}

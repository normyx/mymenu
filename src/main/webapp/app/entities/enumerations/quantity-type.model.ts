export enum QuantityType {
  QUANTITY = 'QUANTITY',

  G = 'G',

  KG = 'KG',

  L = 'L',

  ML = 'ML',

  DC = 'DC',

  SOUPSPOON = 'SOUPSPOON',

  TABLESPOON = 'TABLESPOON',

  TEASPON = 'TEASPON',

  CUP = 'CUP',

  PINCH = 'PINCH',
}

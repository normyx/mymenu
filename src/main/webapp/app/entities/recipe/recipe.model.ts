import dayjs from 'dayjs/esm';
import { ITag } from 'app/entities/tag/tag.model';
import { IIngredient } from 'app/entities/ingredient/ingredient.model';
import { IRecipePicture } from 'app/entities/recipe-picture/recipe-picture.model';
import { IRecipeStep } from 'app/entities/recipe-step/recipe-step.model';
import { PreferredSeason } from 'app/entities/enumerations/preferred-season.model';
import { RecipeType } from 'app/entities/enumerations/recipe-type.model';

export interface IRecipe {
  id?: number;
  name?: string;
  creationDate?: dayjs.Dayjs;
  description?: string | null;
  preparationDuration?: number | null;
  restDuraction?: number | null;
  cookingDuration?: number | null;
  winterPreferrency?: PreferredSeason | null;
  springPreferrency?: PreferredSeason | null;
  summerPreferrency?: PreferredSeason | null;
  autumnPreferrency?: PreferredSeason | null;
  numberOfPeople?: number | null;
  recipeType?: RecipeType;
  recipeURL?: string | null;
  shortSteps?: string | null;
  isShortDescRecipe?: boolean;
  tags?: ITag[] | null;
  ingredients?: IIngredient[] | null;
  recipePictures?: IRecipePicture[] | null;
  recipeSteps?: IRecipeStep[] | null;
}

export class Recipe implements IRecipe {
  constructor(
    public id?: number,
    public name?: string,
    public creationDate?: dayjs.Dayjs,
    public description?: string | null,
    public preparationDuration?: number | null,
    public restDuraction?: number | null,
    public cookingDuration?: number | null,
    public winterPreferrency?: PreferredSeason | null,
    public springPreferrency?: PreferredSeason | null,
    public summerPreferrency?: PreferredSeason | null,
    public autumnPreferrency?: PreferredSeason | null,
    public numberOfPeople?: number | null,
    public recipeType?: RecipeType,
    public recipeURL?: string | null,
    public shortSteps?: string | null,
    public isShortDescRecipe?: boolean,
    public tags?: ITag[] | null,
    public ingredients?: IIngredient[] | null,
    public recipePictures?: IRecipePicture[] | null,
    public recipeSteps?: IRecipeStep[] | null
  ) {
    this.isShortDescRecipe = this.isShortDescRecipe ?? false;
  }
}

export function getRecipeIdentifier(recipe: IRecipe): number | undefined {
  return recipe.id;
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { RecipeService } from '../service/recipe.service';
import { IRecipe, Recipe } from '../recipe.model';
import { ITag } from 'app/entities/tag/tag.model';
import { TagService } from 'app/entities/tag/service/tag.service';
import { IIngredient } from 'app/entities/ingredient/ingredient.model';
import { IngredientService } from 'app/entities/ingredient/service/ingredient.service';
import { IRecipePicture } from 'app/entities/recipe-picture/recipe-picture.model';
import { RecipePictureService } from 'app/entities/recipe-picture/service/recipe-picture.service';
import { IRecipeStep } from 'app/entities/recipe-step/recipe-step.model';
import { RecipeStepService } from 'app/entities/recipe-step/service/recipe-step.service';

import { RecipeUpdateComponent } from './recipe-update.component';

describe('Recipe Management Update Component', () => {
  let comp: RecipeUpdateComponent;
  let fixture: ComponentFixture<RecipeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let recipeService: RecipeService;
  let tagService: TagService;
  let ingredientService: IngredientService;
  let recipePictureService: RecipePictureService;
  let recipeStepService: RecipeStepService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [RecipeUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(RecipeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(RecipeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    recipeService = TestBed.inject(RecipeService);
    tagService = TestBed.inject(TagService);
    ingredientService = TestBed.inject(IngredientService);
    recipePictureService = TestBed.inject(RecipePictureService);
    recipeStepService = TestBed.inject(RecipeStepService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Tag query and add missing value', () => {
      const recipe: IRecipe = { id: 456 };
      const tags: ITag[] = [{ id: 19297 }];
      recipe.tags = tags;

      const tagCollection: ITag[] = [{ id: 83176 }];
      jest.spyOn(tagService, 'query').mockReturnValue(of(new HttpResponse({ body: tagCollection })));
      const additionalTags = [...tags];
      const expectedCollection: ITag[] = [...additionalTags, ...tagCollection];
      jest.spyOn(tagService, 'addTagToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ recipe });
      comp.ngOnInit();

      expect(tagService.query).toHaveBeenCalled();
      expect(tagService.addTagToCollectionIfMissing).toHaveBeenCalledWith(tagCollection, ...additionalTags);
      expect(comp.tagsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Ingredient query and add missing value', () => {
      const recipe: IRecipe = { id: 456 };
      const ingredients: IIngredient[] = [{ id: 10338 }];
      recipe.ingredients = ingredients;

      const ingredientCollection: IIngredient[] = [{ id: 95004 }];
      jest.spyOn(ingredientService, 'query').mockReturnValue(of(new HttpResponse({ body: ingredientCollection })));
      const additionalIngredients = [...ingredients];
      const expectedCollection: IIngredient[] = [...additionalIngredients, ...ingredientCollection];
      jest.spyOn(ingredientService, 'addIngredientToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ recipe });
      comp.ngOnInit();

      expect(ingredientService.query).toHaveBeenCalled();
      expect(ingredientService.addIngredientToCollectionIfMissing).toHaveBeenCalledWith(ingredientCollection, ...additionalIngredients);
      expect(comp.ingredientsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call RecipePicture query and add missing value', () => {
      const recipe: IRecipe = { id: 456 };
      const recipePictures: IRecipePicture[] = [{ id: 26404 }];
      recipe.recipePictures = recipePictures;

      const recipePictureCollection: IRecipePicture[] = [{ id: 77795 }];
      jest.spyOn(recipePictureService, 'query').mockReturnValue(of(new HttpResponse({ body: recipePictureCollection })));
      const additionalRecipePictures = [...recipePictures];
      const expectedCollection: IRecipePicture[] = [...additionalRecipePictures, ...recipePictureCollection];
      jest.spyOn(recipePictureService, 'addRecipePictureToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ recipe });
      comp.ngOnInit();

      expect(recipePictureService.query).toHaveBeenCalled();
      expect(recipePictureService.addRecipePictureToCollectionIfMissing).toHaveBeenCalledWith(
        recipePictureCollection,
        ...additionalRecipePictures
      );
      expect(comp.recipePicturesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call RecipeStep query and add missing value', () => {
      const recipe: IRecipe = { id: 456 };
      const recipeSteps: IRecipeStep[] = [{ id: 82272 }];
      recipe.recipeSteps = recipeSteps;

      const recipeStepCollection: IRecipeStep[] = [{ id: 38304 }];
      jest.spyOn(recipeStepService, 'query').mockReturnValue(of(new HttpResponse({ body: recipeStepCollection })));
      const additionalRecipeSteps = [...recipeSteps];
      const expectedCollection: IRecipeStep[] = [...additionalRecipeSteps, ...recipeStepCollection];
      jest.spyOn(recipeStepService, 'addRecipeStepToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ recipe });
      comp.ngOnInit();

      expect(recipeStepService.query).toHaveBeenCalled();
      expect(recipeStepService.addRecipeStepToCollectionIfMissing).toHaveBeenCalledWith(recipeStepCollection, ...additionalRecipeSteps);
      expect(comp.recipeStepsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const recipe: IRecipe = { id: 456 };
      const tags: ITag = { id: 97455 };
      recipe.tags = [tags];
      const ingredients: IIngredient = { id: 10221 };
      recipe.ingredients = [ingredients];
      const recipePictures: IRecipePicture = { id: 74771 };
      recipe.recipePictures = [recipePictures];
      const recipeSteps: IRecipeStep = { id: 95488 };
      recipe.recipeSteps = [recipeSteps];

      activatedRoute.data = of({ recipe });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(recipe));
      expect(comp.tagsSharedCollection).toContain(tags);
      expect(comp.ingredientsSharedCollection).toContain(ingredients);
      expect(comp.recipePicturesSharedCollection).toContain(recipePictures);
      expect(comp.recipeStepsSharedCollection).toContain(recipeSteps);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Recipe>>();
      const recipe = { id: 123 };
      jest.spyOn(recipeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ recipe });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: recipe }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(recipeService.update).toHaveBeenCalledWith(recipe);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Recipe>>();
      const recipe = new Recipe();
      jest.spyOn(recipeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ recipe });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: recipe }));
      saveSubject.complete();

      // THEN
      expect(recipeService.create).toHaveBeenCalledWith(recipe);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Recipe>>();
      const recipe = { id: 123 };
      jest.spyOn(recipeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ recipe });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(recipeService.update).toHaveBeenCalledWith(recipe);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackTagById', () => {
      it('Should return tracked Tag primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackTagById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackIngredientById', () => {
      it('Should return tracked Ingredient primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackIngredientById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackRecipePictureById', () => {
      it('Should return tracked RecipePicture primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackRecipePictureById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackRecipeStepById', () => {
      it('Should return tracked RecipeStep primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackRecipeStepById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });

  describe('Getting selected relationships', () => {
    describe('getSelectedTag', () => {
      it('Should return option if no Tag is selected', () => {
        const option = { id: 123 };
        const result = comp.getSelectedTag(option);
        expect(result === option).toEqual(true);
      });

      it('Should return selected Tag for according option', () => {
        const option = { id: 123 };
        const selected = { id: 123 };
        const selected2 = { id: 456 };
        const result = comp.getSelectedTag(option, [selected2, selected]);
        expect(result === selected).toEqual(true);
        expect(result === selected2).toEqual(false);
        expect(result === option).toEqual(false);
      });

      it('Should return option if this Tag is not selected', () => {
        const option = { id: 123 };
        const selected = { id: 456 };
        const result = comp.getSelectedTag(option, [selected]);
        expect(result === option).toEqual(true);
        expect(result === selected).toEqual(false);
      });
    });

    describe('getSelectedIngredient', () => {
      it('Should return option if no Ingredient is selected', () => {
        const option = { id: 123 };
        const result = comp.getSelectedIngredient(option);
        expect(result === option).toEqual(true);
      });

      it('Should return selected Ingredient for according option', () => {
        const option = { id: 123 };
        const selected = { id: 123 };
        const selected2 = { id: 456 };
        const result = comp.getSelectedIngredient(option, [selected2, selected]);
        expect(result === selected).toEqual(true);
        expect(result === selected2).toEqual(false);
        expect(result === option).toEqual(false);
      });

      it('Should return option if this Ingredient is not selected', () => {
        const option = { id: 123 };
        const selected = { id: 456 };
        const result = comp.getSelectedIngredient(option, [selected]);
        expect(result === option).toEqual(true);
        expect(result === selected).toEqual(false);
      });
    });

    describe('getSelectedRecipePicture', () => {
      it('Should return option if no RecipePicture is selected', () => {
        const option = { id: 123 };
        const result = comp.getSelectedRecipePicture(option);
        expect(result === option).toEqual(true);
      });

      it('Should return selected RecipePicture for according option', () => {
        const option = { id: 123 };
        const selected = { id: 123 };
        const selected2 = { id: 456 };
        const result = comp.getSelectedRecipePicture(option, [selected2, selected]);
        expect(result === selected).toEqual(true);
        expect(result === selected2).toEqual(false);
        expect(result === option).toEqual(false);
      });

      it('Should return option if this RecipePicture is not selected', () => {
        const option = { id: 123 };
        const selected = { id: 456 };
        const result = comp.getSelectedRecipePicture(option, [selected]);
        expect(result === option).toEqual(true);
        expect(result === selected).toEqual(false);
      });
    });

    describe('getSelectedRecipeStep', () => {
      it('Should return option if no RecipeStep is selected', () => {
        const option = { id: 123 };
        const result = comp.getSelectedRecipeStep(option);
        expect(result === option).toEqual(true);
      });

      it('Should return selected RecipeStep for according option', () => {
        const option = { id: 123 };
        const selected = { id: 123 };
        const selected2 = { id: 456 };
        const result = comp.getSelectedRecipeStep(option, [selected2, selected]);
        expect(result === selected).toEqual(true);
        expect(result === selected2).toEqual(false);
        expect(result === option).toEqual(false);
      });

      it('Should return option if this RecipeStep is not selected', () => {
        const option = { id: 123 };
        const selected = { id: 456 };
        const result = comp.getSelectedRecipeStep(option, [selected]);
        expect(result === option).toEqual(true);
        expect(result === selected).toEqual(false);
      });
    });
  });
});

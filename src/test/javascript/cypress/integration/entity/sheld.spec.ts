import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Sheld e2e test', () => {
  const sheldPageUrl = '/sheld';
  const sheldPageUrlPattern = new RegExp('/sheld(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const sheldSample = { name: 'withdrawal payment Card' };

  let sheld: any;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/shelds+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/shelds').as('postEntityRequest');
    cy.intercept('DELETE', '/api/shelds/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (sheld) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/shelds/${sheld.id}`,
      }).then(() => {
        sheld = undefined;
      });
    }
  });

  it('Shelds menu should load Shelds page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('sheld');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Sheld').should('exist');
    cy.url().should('match', sheldPageUrlPattern);
  });

  describe('Sheld page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(sheldPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Sheld page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/sheld/new$'));
        cy.getEntityCreateUpdateHeading('Sheld');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', sheldPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/shelds',
          body: sheldSample,
        }).then(({ body }) => {
          sheld = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/shelds+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/shelds?page=0&size=20>; rel="last",<http://localhost/api/shelds?page=0&size=20>; rel="first"',
              },
              body: [sheld],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(sheldPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Sheld page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('sheld');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', sheldPageUrlPattern);
      });

      it('edit button click should load edit Sheld page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Sheld');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', sheldPageUrlPattern);
      });

      it('last delete button click should delete instance of Sheld', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('sheld').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', sheldPageUrlPattern);

        sheld = undefined;
      });
    });
  });

  describe('new Sheld page', () => {
    beforeEach(() => {
      cy.visit(`${sheldPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('Sheld');
    });

    it('should create an instance of Sheld', () => {
      cy.get(`[data-cy="name"]`).type('Guarani array Cambridgeshire').should('have.value', 'Guarani array Cambridgeshire');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        sheld = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', sheldPageUrlPattern);
    });
  });
});

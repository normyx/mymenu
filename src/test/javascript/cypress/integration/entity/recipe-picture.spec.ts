import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('RecipePicture e2e test', () => {
  const recipePicturePageUrl = '/recipe-picture';
  const recipePicturePageUrlPattern = new RegExp('/recipe-picture(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const recipePictureSample = { picture: 'Li4vZmFrZS1kYXRhL2Jsb2IvaGlwc3Rlci5wbmc=', pictureContentType: 'unknown', order: 20928 };

  let recipePicture: any;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/recipe-pictures+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/recipe-pictures').as('postEntityRequest');
    cy.intercept('DELETE', '/api/recipe-pictures/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (recipePicture) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/recipe-pictures/${recipePicture.id}`,
      }).then(() => {
        recipePicture = undefined;
      });
    }
  });

  it('RecipePictures menu should load RecipePictures page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('recipe-picture');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('RecipePicture').should('exist');
    cy.url().should('match', recipePicturePageUrlPattern);
  });

  describe('RecipePicture page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(recipePicturePageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create RecipePicture page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/recipe-picture/new$'));
        cy.getEntityCreateUpdateHeading('RecipePicture');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', recipePicturePageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/recipe-pictures',
          body: recipePictureSample,
        }).then(({ body }) => {
          recipePicture = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/recipe-pictures+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/recipe-pictures?page=0&size=20>; rel="last",<http://localhost/api/recipe-pictures?page=0&size=20>; rel="first"',
              },
              body: [recipePicture],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(recipePicturePageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details RecipePicture page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('recipePicture');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', recipePicturePageUrlPattern);
      });

      it('edit button click should load edit RecipePicture page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('RecipePicture');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', recipePicturePageUrlPattern);
      });

      it('last delete button click should delete instance of RecipePicture', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('recipePicture').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', recipePicturePageUrlPattern);

        recipePicture = undefined;
      });
    });
  });

  describe('new RecipePicture page', () => {
    beforeEach(() => {
      cy.visit(`${recipePicturePageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('RecipePicture');
    });

    it('should create an instance of RecipePicture', () => {
      cy.setFieldImageAsBytesOfEntity('picture', 'integration-test.png', 'image/png');

      cy.get(`[data-cy="label"]`).type('Philippine Italie Soft').should('have.value', 'Philippine Italie Soft');

      cy.get(`[data-cy="order"]`).type('5187').should('have.value', '5187');

      // since cypress clicks submit too fast before the blob fields are validated
      cy.wait(200); // eslint-disable-line cypress/no-unnecessary-waiting
      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        recipePicture = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', recipePicturePageUrlPattern);
    });
  });
});

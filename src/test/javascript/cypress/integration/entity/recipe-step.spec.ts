import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('RecipeStep e2e test', () => {
  const recipeStepPageUrl = '/recipe-step';
  const recipeStepPageUrlPattern = new RegExp('/recipe-step(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const recipeStepSample = { stepNum: 77829 };

  let recipeStep: any;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/recipe-steps+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/recipe-steps').as('postEntityRequest');
    cy.intercept('DELETE', '/api/recipe-steps/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (recipeStep) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/recipe-steps/${recipeStep.id}`,
      }).then(() => {
        recipeStep = undefined;
      });
    }
  });

  it('RecipeSteps menu should load RecipeSteps page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('recipe-step');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('RecipeStep').should('exist');
    cy.url().should('match', recipeStepPageUrlPattern);
  });

  describe('RecipeStep page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(recipeStepPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create RecipeStep page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/recipe-step/new$'));
        cy.getEntityCreateUpdateHeading('RecipeStep');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', recipeStepPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/recipe-steps',
          body: recipeStepSample,
        }).then(({ body }) => {
          recipeStep = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/recipe-steps+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/recipe-steps?page=0&size=20>; rel="last",<http://localhost/api/recipe-steps?page=0&size=20>; rel="first"',
              },
              body: [recipeStep],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(recipeStepPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details RecipeStep page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('recipeStep');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', recipeStepPageUrlPattern);
      });

      it('edit button click should load edit RecipeStep page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('RecipeStep');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', recipeStepPageUrlPattern);
      });

      it('last delete button click should delete instance of RecipeStep', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('recipeStep').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', recipeStepPageUrlPattern);

        recipeStep = undefined;
      });
    });
  });

  describe('new RecipeStep page', () => {
    beforeEach(() => {
      cy.visit(`${recipeStepPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('RecipeStep');
    });

    it('should create an instance of RecipeStep', () => {
      cy.get(`[data-cy="stepNum"]`).type('97508').should('have.value', '97508');

      cy.get(`[data-cy="stepDescription"]`).type('quantifying Avon c').should('have.value', 'quantifying Avon c');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        recipeStep = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', recipeStepPageUrlPattern);
    });
  });
});

package com.mgoulene.mymenu.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mgoulene.mymenu.IntegrationTest;
import com.mgoulene.mymenu.domain.Ingredient;
import com.mgoulene.mymenu.domain.Recipe;
import com.mgoulene.mymenu.domain.RecipePicture;
import com.mgoulene.mymenu.domain.RecipeStep;
import com.mgoulene.mymenu.domain.Tag;
import com.mgoulene.mymenu.domain.enumeration.PreferredSeason;
import com.mgoulene.mymenu.domain.enumeration.PreferredSeason;
import com.mgoulene.mymenu.domain.enumeration.PreferredSeason;
import com.mgoulene.mymenu.domain.enumeration.PreferredSeason;
import com.mgoulene.mymenu.domain.enumeration.RecipeType;
import com.mgoulene.mymenu.repository.RecipeRepository;
import com.mgoulene.mymenu.repository.search.RecipeSearchRepository;
import com.mgoulene.mymenu.service.RecipeService;
import com.mgoulene.mymenu.service.criteria.RecipeCriteria;
import com.mgoulene.mymenu.service.dto.RecipeDTO;
import com.mgoulene.mymenu.service.mapper.RecipeMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link RecipeResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class RecipeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_CREATION_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_PREPARATION_DURATION = 0;
    private static final Integer UPDATED_PREPARATION_DURATION = 1;
    private static final Integer SMALLER_PREPARATION_DURATION = 0 - 1;

    private static final Integer DEFAULT_REST_DURACTION = 0;
    private static final Integer UPDATED_REST_DURACTION = 1;
    private static final Integer SMALLER_REST_DURACTION = 0 - 1;

    private static final Integer DEFAULT_COOKING_DURATION = 0;
    private static final Integer UPDATED_COOKING_DURATION = 1;
    private static final Integer SMALLER_COOKING_DURATION = 0 - 1;

    private static final PreferredSeason DEFAULT_WINTER_PREFERRENCY = PreferredSeason.TRUE;
    private static final PreferredSeason UPDATED_WINTER_PREFERRENCY = PreferredSeason.FALSE;

    private static final PreferredSeason DEFAULT_SPRING_PREFERRENCY = PreferredSeason.TRUE;
    private static final PreferredSeason UPDATED_SPRING_PREFERRENCY = PreferredSeason.FALSE;

    private static final PreferredSeason DEFAULT_SUMMER_PREFERRENCY = PreferredSeason.TRUE;
    private static final PreferredSeason UPDATED_SUMMER_PREFERRENCY = PreferredSeason.FALSE;

    private static final PreferredSeason DEFAULT_AUTUMN_PREFERRENCY = PreferredSeason.TRUE;
    private static final PreferredSeason UPDATED_AUTUMN_PREFERRENCY = PreferredSeason.FALSE;

    private static final Integer DEFAULT_NUMBER_OF_PEOPLE = 1;
    private static final Integer UPDATED_NUMBER_OF_PEOPLE = 2;
    private static final Integer SMALLER_NUMBER_OF_PEOPLE = 1 - 1;

    private static final RecipeType DEFAULT_RECIPE_TYPE = RecipeType.APERITIF;
    private static final RecipeType UPDATED_RECIPE_TYPE = RecipeType.STARTER;

    private static final String DEFAULT_RECIPE_URL = "AAAAAAAAAA";
    private static final String UPDATED_RECIPE_URL = "BBBBBBBBBB";

    private static final String DEFAULT_SHORT_STEPS = "AAAAAAAAAA";
    private static final String UPDATED_SHORT_STEPS = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_SHORT_DESC_RECIPE = false;
    private static final Boolean UPDATED_IS_SHORT_DESC_RECIPE = true;

    private static final String ENTITY_API_URL = "/api/recipes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/recipes";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private RecipeRepository recipeRepository;

    @Mock
    private RecipeRepository recipeRepositoryMock;

    @Autowired
    private RecipeMapper recipeMapper;

    @Mock
    private RecipeService recipeServiceMock;

    /**
     * This repository is mocked in the com.mgoulene.mymenu.repository.search test package.
     *
     * @see com.mgoulene.mymenu.repository.search.RecipeSearchRepositoryMockConfiguration
     */
    @Autowired
    private RecipeSearchRepository mockRecipeSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRecipeMockMvc;

    private Recipe recipe;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Recipe createEntity(EntityManager em) {
        Recipe recipe = new Recipe()
            .name(DEFAULT_NAME)
            .creationDate(DEFAULT_CREATION_DATE)
            .description(DEFAULT_DESCRIPTION)
            .preparationDuration(DEFAULT_PREPARATION_DURATION)
            .restDuraction(DEFAULT_REST_DURACTION)
            .cookingDuration(DEFAULT_COOKING_DURATION)
            .winterPreferrency(DEFAULT_WINTER_PREFERRENCY)
            .springPreferrency(DEFAULT_SPRING_PREFERRENCY)
            .summerPreferrency(DEFAULT_SUMMER_PREFERRENCY)
            .autumnPreferrency(DEFAULT_AUTUMN_PREFERRENCY)
            .numberOfPeople(DEFAULT_NUMBER_OF_PEOPLE)
            .recipeType(DEFAULT_RECIPE_TYPE)
            .recipeURL(DEFAULT_RECIPE_URL)
            .shortSteps(DEFAULT_SHORT_STEPS)
            .isShortDescRecipe(DEFAULT_IS_SHORT_DESC_RECIPE);
        return recipe;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Recipe createUpdatedEntity(EntityManager em) {
        Recipe recipe = new Recipe()
            .name(UPDATED_NAME)
            .creationDate(UPDATED_CREATION_DATE)
            .description(UPDATED_DESCRIPTION)
            .preparationDuration(UPDATED_PREPARATION_DURATION)
            .restDuraction(UPDATED_REST_DURACTION)
            .cookingDuration(UPDATED_COOKING_DURATION)
            .winterPreferrency(UPDATED_WINTER_PREFERRENCY)
            .springPreferrency(UPDATED_SPRING_PREFERRENCY)
            .summerPreferrency(UPDATED_SUMMER_PREFERRENCY)
            .autumnPreferrency(UPDATED_AUTUMN_PREFERRENCY)
            .numberOfPeople(UPDATED_NUMBER_OF_PEOPLE)
            .recipeType(UPDATED_RECIPE_TYPE)
            .recipeURL(UPDATED_RECIPE_URL)
            .shortSteps(UPDATED_SHORT_STEPS)
            .isShortDescRecipe(UPDATED_IS_SHORT_DESC_RECIPE);
        return recipe;
    }

    @BeforeEach
    public void initTest() {
        recipe = createEntity(em);
    }

    @Test
    @Transactional
    void createRecipe() throws Exception {
        int databaseSizeBeforeCreate = recipeRepository.findAll().size();
        // Create the Recipe
        RecipeDTO recipeDTO = recipeMapper.toDto(recipe);
        restRecipeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(recipeDTO)))
            .andExpect(status().isCreated());

        // Validate the Recipe in the database
        List<Recipe> recipeList = recipeRepository.findAll();
        assertThat(recipeList).hasSize(databaseSizeBeforeCreate + 1);
        Recipe testRecipe = recipeList.get(recipeList.size() - 1);
        assertThat(testRecipe.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testRecipe.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testRecipe.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testRecipe.getPreparationDuration()).isEqualTo(DEFAULT_PREPARATION_DURATION);
        assertThat(testRecipe.getRestDuraction()).isEqualTo(DEFAULT_REST_DURACTION);
        assertThat(testRecipe.getCookingDuration()).isEqualTo(DEFAULT_COOKING_DURATION);
        assertThat(testRecipe.getWinterPreferrency()).isEqualTo(DEFAULT_WINTER_PREFERRENCY);
        assertThat(testRecipe.getSpringPreferrency()).isEqualTo(DEFAULT_SPRING_PREFERRENCY);
        assertThat(testRecipe.getSummerPreferrency()).isEqualTo(DEFAULT_SUMMER_PREFERRENCY);
        assertThat(testRecipe.getAutumnPreferrency()).isEqualTo(DEFAULT_AUTUMN_PREFERRENCY);
        assertThat(testRecipe.getNumberOfPeople()).isEqualTo(DEFAULT_NUMBER_OF_PEOPLE);
        assertThat(testRecipe.getRecipeType()).isEqualTo(DEFAULT_RECIPE_TYPE);
        assertThat(testRecipe.getRecipeURL()).isEqualTo(DEFAULT_RECIPE_URL);
        assertThat(testRecipe.getShortSteps()).isEqualTo(DEFAULT_SHORT_STEPS);
        assertThat(testRecipe.getIsShortDescRecipe()).isEqualTo(DEFAULT_IS_SHORT_DESC_RECIPE);

        // Validate the Recipe in Elasticsearch
        verify(mockRecipeSearchRepository, times(1)).save(testRecipe);
    }

    @Test
    @Transactional
    void createRecipeWithExistingId() throws Exception {
        // Create the Recipe with an existing ID
        recipe.setId(1L);
        RecipeDTO recipeDTO = recipeMapper.toDto(recipe);

        int databaseSizeBeforeCreate = recipeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restRecipeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(recipeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Recipe in the database
        List<Recipe> recipeList = recipeRepository.findAll();
        assertThat(recipeList).hasSize(databaseSizeBeforeCreate);

        // Validate the Recipe in Elasticsearch
        verify(mockRecipeSearchRepository, times(0)).save(recipe);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = recipeRepository.findAll().size();
        // set the field null
        recipe.setName(null);

        // Create the Recipe, which fails.
        RecipeDTO recipeDTO = recipeMapper.toDto(recipe);

        restRecipeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(recipeDTO)))
            .andExpect(status().isBadRequest());

        List<Recipe> recipeList = recipeRepository.findAll();
        assertThat(recipeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreationDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = recipeRepository.findAll().size();
        // set the field null
        recipe.setCreationDate(null);

        // Create the Recipe, which fails.
        RecipeDTO recipeDTO = recipeMapper.toDto(recipe);

        restRecipeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(recipeDTO)))
            .andExpect(status().isBadRequest());

        List<Recipe> recipeList = recipeRepository.findAll();
        assertThat(recipeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRecipeTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = recipeRepository.findAll().size();
        // set the field null
        recipe.setRecipeType(null);

        // Create the Recipe, which fails.
        RecipeDTO recipeDTO = recipeMapper.toDto(recipe);

        restRecipeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(recipeDTO)))
            .andExpect(status().isBadRequest());

        List<Recipe> recipeList = recipeRepository.findAll();
        assertThat(recipeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkIsShortDescRecipeIsRequired() throws Exception {
        int databaseSizeBeforeTest = recipeRepository.findAll().size();
        // set the field null
        recipe.setIsShortDescRecipe(null);

        // Create the Recipe, which fails.
        RecipeDTO recipeDTO = recipeMapper.toDto(recipe);

        restRecipeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(recipeDTO)))
            .andExpect(status().isBadRequest());

        List<Recipe> recipeList = recipeRepository.findAll();
        assertThat(recipeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllRecipes() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList
        restRecipeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recipe.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].preparationDuration").value(hasItem(DEFAULT_PREPARATION_DURATION)))
            .andExpect(jsonPath("$.[*].restDuraction").value(hasItem(DEFAULT_REST_DURACTION)))
            .andExpect(jsonPath("$.[*].cookingDuration").value(hasItem(DEFAULT_COOKING_DURATION)))
            .andExpect(jsonPath("$.[*].winterPreferrency").value(hasItem(DEFAULT_WINTER_PREFERRENCY.toString())))
            .andExpect(jsonPath("$.[*].springPreferrency").value(hasItem(DEFAULT_SPRING_PREFERRENCY.toString())))
            .andExpect(jsonPath("$.[*].summerPreferrency").value(hasItem(DEFAULT_SUMMER_PREFERRENCY.toString())))
            .andExpect(jsonPath("$.[*].autumnPreferrency").value(hasItem(DEFAULT_AUTUMN_PREFERRENCY.toString())))
            .andExpect(jsonPath("$.[*].numberOfPeople").value(hasItem(DEFAULT_NUMBER_OF_PEOPLE)))
            .andExpect(jsonPath("$.[*].recipeType").value(hasItem(DEFAULT_RECIPE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].recipeURL").value(hasItem(DEFAULT_RECIPE_URL)))
            .andExpect(jsonPath("$.[*].shortSteps").value(hasItem(DEFAULT_SHORT_STEPS)))
            .andExpect(jsonPath("$.[*].isShortDescRecipe").value(hasItem(DEFAULT_IS_SHORT_DESC_RECIPE.booleanValue())));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllRecipesWithEagerRelationshipsIsEnabled() throws Exception {
        when(recipeServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restRecipeMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(recipeServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllRecipesWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(recipeServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restRecipeMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(recipeServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    void getRecipe() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get the recipe
        restRecipeMockMvc
            .perform(get(ENTITY_API_URL_ID, recipe.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(recipe.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.preparationDuration").value(DEFAULT_PREPARATION_DURATION))
            .andExpect(jsonPath("$.restDuraction").value(DEFAULT_REST_DURACTION))
            .andExpect(jsonPath("$.cookingDuration").value(DEFAULT_COOKING_DURATION))
            .andExpect(jsonPath("$.winterPreferrency").value(DEFAULT_WINTER_PREFERRENCY.toString()))
            .andExpect(jsonPath("$.springPreferrency").value(DEFAULT_SPRING_PREFERRENCY.toString()))
            .andExpect(jsonPath("$.summerPreferrency").value(DEFAULT_SUMMER_PREFERRENCY.toString()))
            .andExpect(jsonPath("$.autumnPreferrency").value(DEFAULT_AUTUMN_PREFERRENCY.toString()))
            .andExpect(jsonPath("$.numberOfPeople").value(DEFAULT_NUMBER_OF_PEOPLE))
            .andExpect(jsonPath("$.recipeType").value(DEFAULT_RECIPE_TYPE.toString()))
            .andExpect(jsonPath("$.recipeURL").value(DEFAULT_RECIPE_URL))
            .andExpect(jsonPath("$.shortSteps").value(DEFAULT_SHORT_STEPS))
            .andExpect(jsonPath("$.isShortDescRecipe").value(DEFAULT_IS_SHORT_DESC_RECIPE.booleanValue()));
    }

    @Test
    @Transactional
    void getRecipesByIdFiltering() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        Long id = recipe.getId();

        defaultRecipeShouldBeFound("id.equals=" + id);
        defaultRecipeShouldNotBeFound("id.notEquals=" + id);

        defaultRecipeShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultRecipeShouldNotBeFound("id.greaterThan=" + id);

        defaultRecipeShouldBeFound("id.lessThanOrEqual=" + id);
        defaultRecipeShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllRecipesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where name equals to DEFAULT_NAME
        defaultRecipeShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the recipeList where name equals to UPDATED_NAME
        defaultRecipeShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllRecipesByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where name not equals to DEFAULT_NAME
        defaultRecipeShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the recipeList where name not equals to UPDATED_NAME
        defaultRecipeShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllRecipesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where name in DEFAULT_NAME or UPDATED_NAME
        defaultRecipeShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the recipeList where name equals to UPDATED_NAME
        defaultRecipeShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllRecipesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where name is not null
        defaultRecipeShouldBeFound("name.specified=true");

        // Get all the recipeList where name is null
        defaultRecipeShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllRecipesByNameContainsSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where name contains DEFAULT_NAME
        defaultRecipeShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the recipeList where name contains UPDATED_NAME
        defaultRecipeShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllRecipesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where name does not contain DEFAULT_NAME
        defaultRecipeShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the recipeList where name does not contain UPDATED_NAME
        defaultRecipeShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllRecipesByCreationDateIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where creationDate equals to DEFAULT_CREATION_DATE
        defaultRecipeShouldBeFound("creationDate.equals=" + DEFAULT_CREATION_DATE);

        // Get all the recipeList where creationDate equals to UPDATED_CREATION_DATE
        defaultRecipeShouldNotBeFound("creationDate.equals=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    void getAllRecipesByCreationDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where creationDate not equals to DEFAULT_CREATION_DATE
        defaultRecipeShouldNotBeFound("creationDate.notEquals=" + DEFAULT_CREATION_DATE);

        // Get all the recipeList where creationDate not equals to UPDATED_CREATION_DATE
        defaultRecipeShouldBeFound("creationDate.notEquals=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    void getAllRecipesByCreationDateIsInShouldWork() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where creationDate in DEFAULT_CREATION_DATE or UPDATED_CREATION_DATE
        defaultRecipeShouldBeFound("creationDate.in=" + DEFAULT_CREATION_DATE + "," + UPDATED_CREATION_DATE);

        // Get all the recipeList where creationDate equals to UPDATED_CREATION_DATE
        defaultRecipeShouldNotBeFound("creationDate.in=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    void getAllRecipesByCreationDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where creationDate is not null
        defaultRecipeShouldBeFound("creationDate.specified=true");

        // Get all the recipeList where creationDate is null
        defaultRecipeShouldNotBeFound("creationDate.specified=false");
    }

    @Test
    @Transactional
    void getAllRecipesByCreationDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where creationDate is greater than or equal to DEFAULT_CREATION_DATE
        defaultRecipeShouldBeFound("creationDate.greaterThanOrEqual=" + DEFAULT_CREATION_DATE);

        // Get all the recipeList where creationDate is greater than or equal to UPDATED_CREATION_DATE
        defaultRecipeShouldNotBeFound("creationDate.greaterThanOrEqual=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    void getAllRecipesByCreationDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where creationDate is less than or equal to DEFAULT_CREATION_DATE
        defaultRecipeShouldBeFound("creationDate.lessThanOrEqual=" + DEFAULT_CREATION_DATE);

        // Get all the recipeList where creationDate is less than or equal to SMALLER_CREATION_DATE
        defaultRecipeShouldNotBeFound("creationDate.lessThanOrEqual=" + SMALLER_CREATION_DATE);
    }

    @Test
    @Transactional
    void getAllRecipesByCreationDateIsLessThanSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where creationDate is less than DEFAULT_CREATION_DATE
        defaultRecipeShouldNotBeFound("creationDate.lessThan=" + DEFAULT_CREATION_DATE);

        // Get all the recipeList where creationDate is less than UPDATED_CREATION_DATE
        defaultRecipeShouldBeFound("creationDate.lessThan=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    void getAllRecipesByCreationDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where creationDate is greater than DEFAULT_CREATION_DATE
        defaultRecipeShouldNotBeFound("creationDate.greaterThan=" + DEFAULT_CREATION_DATE);

        // Get all the recipeList where creationDate is greater than SMALLER_CREATION_DATE
        defaultRecipeShouldBeFound("creationDate.greaterThan=" + SMALLER_CREATION_DATE);
    }

    @Test
    @Transactional
    void getAllRecipesByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where description equals to DEFAULT_DESCRIPTION
        defaultRecipeShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the recipeList where description equals to UPDATED_DESCRIPTION
        defaultRecipeShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllRecipesByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where description not equals to DEFAULT_DESCRIPTION
        defaultRecipeShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the recipeList where description not equals to UPDATED_DESCRIPTION
        defaultRecipeShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllRecipesByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultRecipeShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the recipeList where description equals to UPDATED_DESCRIPTION
        defaultRecipeShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllRecipesByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where description is not null
        defaultRecipeShouldBeFound("description.specified=true");

        // Get all the recipeList where description is null
        defaultRecipeShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    void getAllRecipesByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where description contains DEFAULT_DESCRIPTION
        defaultRecipeShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the recipeList where description contains UPDATED_DESCRIPTION
        defaultRecipeShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllRecipesByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where description does not contain DEFAULT_DESCRIPTION
        defaultRecipeShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the recipeList where description does not contain UPDATED_DESCRIPTION
        defaultRecipeShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllRecipesByPreparationDurationIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where preparationDuration equals to DEFAULT_PREPARATION_DURATION
        defaultRecipeShouldBeFound("preparationDuration.equals=" + DEFAULT_PREPARATION_DURATION);

        // Get all the recipeList where preparationDuration equals to UPDATED_PREPARATION_DURATION
        defaultRecipeShouldNotBeFound("preparationDuration.equals=" + UPDATED_PREPARATION_DURATION);
    }

    @Test
    @Transactional
    void getAllRecipesByPreparationDurationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where preparationDuration not equals to DEFAULT_PREPARATION_DURATION
        defaultRecipeShouldNotBeFound("preparationDuration.notEquals=" + DEFAULT_PREPARATION_DURATION);

        // Get all the recipeList where preparationDuration not equals to UPDATED_PREPARATION_DURATION
        defaultRecipeShouldBeFound("preparationDuration.notEquals=" + UPDATED_PREPARATION_DURATION);
    }

    @Test
    @Transactional
    void getAllRecipesByPreparationDurationIsInShouldWork() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where preparationDuration in DEFAULT_PREPARATION_DURATION or UPDATED_PREPARATION_DURATION
        defaultRecipeShouldBeFound("preparationDuration.in=" + DEFAULT_PREPARATION_DURATION + "," + UPDATED_PREPARATION_DURATION);

        // Get all the recipeList where preparationDuration equals to UPDATED_PREPARATION_DURATION
        defaultRecipeShouldNotBeFound("preparationDuration.in=" + UPDATED_PREPARATION_DURATION);
    }

    @Test
    @Transactional
    void getAllRecipesByPreparationDurationIsNullOrNotNull() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where preparationDuration is not null
        defaultRecipeShouldBeFound("preparationDuration.specified=true");

        // Get all the recipeList where preparationDuration is null
        defaultRecipeShouldNotBeFound("preparationDuration.specified=false");
    }

    @Test
    @Transactional
    void getAllRecipesByPreparationDurationIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where preparationDuration is greater than or equal to DEFAULT_PREPARATION_DURATION
        defaultRecipeShouldBeFound("preparationDuration.greaterThanOrEqual=" + DEFAULT_PREPARATION_DURATION);

        // Get all the recipeList where preparationDuration is greater than or equal to UPDATED_PREPARATION_DURATION
        defaultRecipeShouldNotBeFound("preparationDuration.greaterThanOrEqual=" + UPDATED_PREPARATION_DURATION);
    }

    @Test
    @Transactional
    void getAllRecipesByPreparationDurationIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where preparationDuration is less than or equal to DEFAULT_PREPARATION_DURATION
        defaultRecipeShouldBeFound("preparationDuration.lessThanOrEqual=" + DEFAULT_PREPARATION_DURATION);

        // Get all the recipeList where preparationDuration is less than or equal to SMALLER_PREPARATION_DURATION
        defaultRecipeShouldNotBeFound("preparationDuration.lessThanOrEqual=" + SMALLER_PREPARATION_DURATION);
    }

    @Test
    @Transactional
    void getAllRecipesByPreparationDurationIsLessThanSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where preparationDuration is less than DEFAULT_PREPARATION_DURATION
        defaultRecipeShouldNotBeFound("preparationDuration.lessThan=" + DEFAULT_PREPARATION_DURATION);

        // Get all the recipeList where preparationDuration is less than UPDATED_PREPARATION_DURATION
        defaultRecipeShouldBeFound("preparationDuration.lessThan=" + UPDATED_PREPARATION_DURATION);
    }

    @Test
    @Transactional
    void getAllRecipesByPreparationDurationIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where preparationDuration is greater than DEFAULT_PREPARATION_DURATION
        defaultRecipeShouldNotBeFound("preparationDuration.greaterThan=" + DEFAULT_PREPARATION_DURATION);

        // Get all the recipeList where preparationDuration is greater than SMALLER_PREPARATION_DURATION
        defaultRecipeShouldBeFound("preparationDuration.greaterThan=" + SMALLER_PREPARATION_DURATION);
    }

    @Test
    @Transactional
    void getAllRecipesByRestDuractionIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where restDuraction equals to DEFAULT_REST_DURACTION
        defaultRecipeShouldBeFound("restDuraction.equals=" + DEFAULT_REST_DURACTION);

        // Get all the recipeList where restDuraction equals to UPDATED_REST_DURACTION
        defaultRecipeShouldNotBeFound("restDuraction.equals=" + UPDATED_REST_DURACTION);
    }

    @Test
    @Transactional
    void getAllRecipesByRestDuractionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where restDuraction not equals to DEFAULT_REST_DURACTION
        defaultRecipeShouldNotBeFound("restDuraction.notEquals=" + DEFAULT_REST_DURACTION);

        // Get all the recipeList where restDuraction not equals to UPDATED_REST_DURACTION
        defaultRecipeShouldBeFound("restDuraction.notEquals=" + UPDATED_REST_DURACTION);
    }

    @Test
    @Transactional
    void getAllRecipesByRestDuractionIsInShouldWork() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where restDuraction in DEFAULT_REST_DURACTION or UPDATED_REST_DURACTION
        defaultRecipeShouldBeFound("restDuraction.in=" + DEFAULT_REST_DURACTION + "," + UPDATED_REST_DURACTION);

        // Get all the recipeList where restDuraction equals to UPDATED_REST_DURACTION
        defaultRecipeShouldNotBeFound("restDuraction.in=" + UPDATED_REST_DURACTION);
    }

    @Test
    @Transactional
    void getAllRecipesByRestDuractionIsNullOrNotNull() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where restDuraction is not null
        defaultRecipeShouldBeFound("restDuraction.specified=true");

        // Get all the recipeList where restDuraction is null
        defaultRecipeShouldNotBeFound("restDuraction.specified=false");
    }

    @Test
    @Transactional
    void getAllRecipesByRestDuractionIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where restDuraction is greater than or equal to DEFAULT_REST_DURACTION
        defaultRecipeShouldBeFound("restDuraction.greaterThanOrEqual=" + DEFAULT_REST_DURACTION);

        // Get all the recipeList where restDuraction is greater than or equal to UPDATED_REST_DURACTION
        defaultRecipeShouldNotBeFound("restDuraction.greaterThanOrEqual=" + UPDATED_REST_DURACTION);
    }

    @Test
    @Transactional
    void getAllRecipesByRestDuractionIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where restDuraction is less than or equal to DEFAULT_REST_DURACTION
        defaultRecipeShouldBeFound("restDuraction.lessThanOrEqual=" + DEFAULT_REST_DURACTION);

        // Get all the recipeList where restDuraction is less than or equal to SMALLER_REST_DURACTION
        defaultRecipeShouldNotBeFound("restDuraction.lessThanOrEqual=" + SMALLER_REST_DURACTION);
    }

    @Test
    @Transactional
    void getAllRecipesByRestDuractionIsLessThanSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where restDuraction is less than DEFAULT_REST_DURACTION
        defaultRecipeShouldNotBeFound("restDuraction.lessThan=" + DEFAULT_REST_DURACTION);

        // Get all the recipeList where restDuraction is less than UPDATED_REST_DURACTION
        defaultRecipeShouldBeFound("restDuraction.lessThan=" + UPDATED_REST_DURACTION);
    }

    @Test
    @Transactional
    void getAllRecipesByRestDuractionIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where restDuraction is greater than DEFAULT_REST_DURACTION
        defaultRecipeShouldNotBeFound("restDuraction.greaterThan=" + DEFAULT_REST_DURACTION);

        // Get all the recipeList where restDuraction is greater than SMALLER_REST_DURACTION
        defaultRecipeShouldBeFound("restDuraction.greaterThan=" + SMALLER_REST_DURACTION);
    }

    @Test
    @Transactional
    void getAllRecipesByCookingDurationIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where cookingDuration equals to DEFAULT_COOKING_DURATION
        defaultRecipeShouldBeFound("cookingDuration.equals=" + DEFAULT_COOKING_DURATION);

        // Get all the recipeList where cookingDuration equals to UPDATED_COOKING_DURATION
        defaultRecipeShouldNotBeFound("cookingDuration.equals=" + UPDATED_COOKING_DURATION);
    }

    @Test
    @Transactional
    void getAllRecipesByCookingDurationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where cookingDuration not equals to DEFAULT_COOKING_DURATION
        defaultRecipeShouldNotBeFound("cookingDuration.notEquals=" + DEFAULT_COOKING_DURATION);

        // Get all the recipeList where cookingDuration not equals to UPDATED_COOKING_DURATION
        defaultRecipeShouldBeFound("cookingDuration.notEquals=" + UPDATED_COOKING_DURATION);
    }

    @Test
    @Transactional
    void getAllRecipesByCookingDurationIsInShouldWork() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where cookingDuration in DEFAULT_COOKING_DURATION or UPDATED_COOKING_DURATION
        defaultRecipeShouldBeFound("cookingDuration.in=" + DEFAULT_COOKING_DURATION + "," + UPDATED_COOKING_DURATION);

        // Get all the recipeList where cookingDuration equals to UPDATED_COOKING_DURATION
        defaultRecipeShouldNotBeFound("cookingDuration.in=" + UPDATED_COOKING_DURATION);
    }

    @Test
    @Transactional
    void getAllRecipesByCookingDurationIsNullOrNotNull() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where cookingDuration is not null
        defaultRecipeShouldBeFound("cookingDuration.specified=true");

        // Get all the recipeList where cookingDuration is null
        defaultRecipeShouldNotBeFound("cookingDuration.specified=false");
    }

    @Test
    @Transactional
    void getAllRecipesByCookingDurationIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where cookingDuration is greater than or equal to DEFAULT_COOKING_DURATION
        defaultRecipeShouldBeFound("cookingDuration.greaterThanOrEqual=" + DEFAULT_COOKING_DURATION);

        // Get all the recipeList where cookingDuration is greater than or equal to UPDATED_COOKING_DURATION
        defaultRecipeShouldNotBeFound("cookingDuration.greaterThanOrEqual=" + UPDATED_COOKING_DURATION);
    }

    @Test
    @Transactional
    void getAllRecipesByCookingDurationIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where cookingDuration is less than or equal to DEFAULT_COOKING_DURATION
        defaultRecipeShouldBeFound("cookingDuration.lessThanOrEqual=" + DEFAULT_COOKING_DURATION);

        // Get all the recipeList where cookingDuration is less than or equal to SMALLER_COOKING_DURATION
        defaultRecipeShouldNotBeFound("cookingDuration.lessThanOrEqual=" + SMALLER_COOKING_DURATION);
    }

    @Test
    @Transactional
    void getAllRecipesByCookingDurationIsLessThanSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where cookingDuration is less than DEFAULT_COOKING_DURATION
        defaultRecipeShouldNotBeFound("cookingDuration.lessThan=" + DEFAULT_COOKING_DURATION);

        // Get all the recipeList where cookingDuration is less than UPDATED_COOKING_DURATION
        defaultRecipeShouldBeFound("cookingDuration.lessThan=" + UPDATED_COOKING_DURATION);
    }

    @Test
    @Transactional
    void getAllRecipesByCookingDurationIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where cookingDuration is greater than DEFAULT_COOKING_DURATION
        defaultRecipeShouldNotBeFound("cookingDuration.greaterThan=" + DEFAULT_COOKING_DURATION);

        // Get all the recipeList where cookingDuration is greater than SMALLER_COOKING_DURATION
        defaultRecipeShouldBeFound("cookingDuration.greaterThan=" + SMALLER_COOKING_DURATION);
    }

    @Test
    @Transactional
    void getAllRecipesByWinterPreferrencyIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where winterPreferrency equals to DEFAULT_WINTER_PREFERRENCY
        defaultRecipeShouldBeFound("winterPreferrency.equals=" + DEFAULT_WINTER_PREFERRENCY);

        // Get all the recipeList where winterPreferrency equals to UPDATED_WINTER_PREFERRENCY
        defaultRecipeShouldNotBeFound("winterPreferrency.equals=" + UPDATED_WINTER_PREFERRENCY);
    }

    @Test
    @Transactional
    void getAllRecipesByWinterPreferrencyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where winterPreferrency not equals to DEFAULT_WINTER_PREFERRENCY
        defaultRecipeShouldNotBeFound("winterPreferrency.notEquals=" + DEFAULT_WINTER_PREFERRENCY);

        // Get all the recipeList where winterPreferrency not equals to UPDATED_WINTER_PREFERRENCY
        defaultRecipeShouldBeFound("winterPreferrency.notEquals=" + UPDATED_WINTER_PREFERRENCY);
    }

    @Test
    @Transactional
    void getAllRecipesByWinterPreferrencyIsInShouldWork() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where winterPreferrency in DEFAULT_WINTER_PREFERRENCY or UPDATED_WINTER_PREFERRENCY
        defaultRecipeShouldBeFound("winterPreferrency.in=" + DEFAULT_WINTER_PREFERRENCY + "," + UPDATED_WINTER_PREFERRENCY);

        // Get all the recipeList where winterPreferrency equals to UPDATED_WINTER_PREFERRENCY
        defaultRecipeShouldNotBeFound("winterPreferrency.in=" + UPDATED_WINTER_PREFERRENCY);
    }

    @Test
    @Transactional
    void getAllRecipesByWinterPreferrencyIsNullOrNotNull() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where winterPreferrency is not null
        defaultRecipeShouldBeFound("winterPreferrency.specified=true");

        // Get all the recipeList where winterPreferrency is null
        defaultRecipeShouldNotBeFound("winterPreferrency.specified=false");
    }

    @Test
    @Transactional
    void getAllRecipesBySpringPreferrencyIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where springPreferrency equals to DEFAULT_SPRING_PREFERRENCY
        defaultRecipeShouldBeFound("springPreferrency.equals=" + DEFAULT_SPRING_PREFERRENCY);

        // Get all the recipeList where springPreferrency equals to UPDATED_SPRING_PREFERRENCY
        defaultRecipeShouldNotBeFound("springPreferrency.equals=" + UPDATED_SPRING_PREFERRENCY);
    }

    @Test
    @Transactional
    void getAllRecipesBySpringPreferrencyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where springPreferrency not equals to DEFAULT_SPRING_PREFERRENCY
        defaultRecipeShouldNotBeFound("springPreferrency.notEquals=" + DEFAULT_SPRING_PREFERRENCY);

        // Get all the recipeList where springPreferrency not equals to UPDATED_SPRING_PREFERRENCY
        defaultRecipeShouldBeFound("springPreferrency.notEquals=" + UPDATED_SPRING_PREFERRENCY);
    }

    @Test
    @Transactional
    void getAllRecipesBySpringPreferrencyIsInShouldWork() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where springPreferrency in DEFAULT_SPRING_PREFERRENCY or UPDATED_SPRING_PREFERRENCY
        defaultRecipeShouldBeFound("springPreferrency.in=" + DEFAULT_SPRING_PREFERRENCY + "," + UPDATED_SPRING_PREFERRENCY);

        // Get all the recipeList where springPreferrency equals to UPDATED_SPRING_PREFERRENCY
        defaultRecipeShouldNotBeFound("springPreferrency.in=" + UPDATED_SPRING_PREFERRENCY);
    }

    @Test
    @Transactional
    void getAllRecipesBySpringPreferrencyIsNullOrNotNull() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where springPreferrency is not null
        defaultRecipeShouldBeFound("springPreferrency.specified=true");

        // Get all the recipeList where springPreferrency is null
        defaultRecipeShouldNotBeFound("springPreferrency.specified=false");
    }

    @Test
    @Transactional
    void getAllRecipesBySummerPreferrencyIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where summerPreferrency equals to DEFAULT_SUMMER_PREFERRENCY
        defaultRecipeShouldBeFound("summerPreferrency.equals=" + DEFAULT_SUMMER_PREFERRENCY);

        // Get all the recipeList where summerPreferrency equals to UPDATED_SUMMER_PREFERRENCY
        defaultRecipeShouldNotBeFound("summerPreferrency.equals=" + UPDATED_SUMMER_PREFERRENCY);
    }

    @Test
    @Transactional
    void getAllRecipesBySummerPreferrencyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where summerPreferrency not equals to DEFAULT_SUMMER_PREFERRENCY
        defaultRecipeShouldNotBeFound("summerPreferrency.notEquals=" + DEFAULT_SUMMER_PREFERRENCY);

        // Get all the recipeList where summerPreferrency not equals to UPDATED_SUMMER_PREFERRENCY
        defaultRecipeShouldBeFound("summerPreferrency.notEquals=" + UPDATED_SUMMER_PREFERRENCY);
    }

    @Test
    @Transactional
    void getAllRecipesBySummerPreferrencyIsInShouldWork() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where summerPreferrency in DEFAULT_SUMMER_PREFERRENCY or UPDATED_SUMMER_PREFERRENCY
        defaultRecipeShouldBeFound("summerPreferrency.in=" + DEFAULT_SUMMER_PREFERRENCY + "," + UPDATED_SUMMER_PREFERRENCY);

        // Get all the recipeList where summerPreferrency equals to UPDATED_SUMMER_PREFERRENCY
        defaultRecipeShouldNotBeFound("summerPreferrency.in=" + UPDATED_SUMMER_PREFERRENCY);
    }

    @Test
    @Transactional
    void getAllRecipesBySummerPreferrencyIsNullOrNotNull() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where summerPreferrency is not null
        defaultRecipeShouldBeFound("summerPreferrency.specified=true");

        // Get all the recipeList where summerPreferrency is null
        defaultRecipeShouldNotBeFound("summerPreferrency.specified=false");
    }

    @Test
    @Transactional
    void getAllRecipesByAutumnPreferrencyIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where autumnPreferrency equals to DEFAULT_AUTUMN_PREFERRENCY
        defaultRecipeShouldBeFound("autumnPreferrency.equals=" + DEFAULT_AUTUMN_PREFERRENCY);

        // Get all the recipeList where autumnPreferrency equals to UPDATED_AUTUMN_PREFERRENCY
        defaultRecipeShouldNotBeFound("autumnPreferrency.equals=" + UPDATED_AUTUMN_PREFERRENCY);
    }

    @Test
    @Transactional
    void getAllRecipesByAutumnPreferrencyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where autumnPreferrency not equals to DEFAULT_AUTUMN_PREFERRENCY
        defaultRecipeShouldNotBeFound("autumnPreferrency.notEquals=" + DEFAULT_AUTUMN_PREFERRENCY);

        // Get all the recipeList where autumnPreferrency not equals to UPDATED_AUTUMN_PREFERRENCY
        defaultRecipeShouldBeFound("autumnPreferrency.notEquals=" + UPDATED_AUTUMN_PREFERRENCY);
    }

    @Test
    @Transactional
    void getAllRecipesByAutumnPreferrencyIsInShouldWork() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where autumnPreferrency in DEFAULT_AUTUMN_PREFERRENCY or UPDATED_AUTUMN_PREFERRENCY
        defaultRecipeShouldBeFound("autumnPreferrency.in=" + DEFAULT_AUTUMN_PREFERRENCY + "," + UPDATED_AUTUMN_PREFERRENCY);

        // Get all the recipeList where autumnPreferrency equals to UPDATED_AUTUMN_PREFERRENCY
        defaultRecipeShouldNotBeFound("autumnPreferrency.in=" + UPDATED_AUTUMN_PREFERRENCY);
    }

    @Test
    @Transactional
    void getAllRecipesByAutumnPreferrencyIsNullOrNotNull() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where autumnPreferrency is not null
        defaultRecipeShouldBeFound("autumnPreferrency.specified=true");

        // Get all the recipeList where autumnPreferrency is null
        defaultRecipeShouldNotBeFound("autumnPreferrency.specified=false");
    }

    @Test
    @Transactional
    void getAllRecipesByNumberOfPeopleIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where numberOfPeople equals to DEFAULT_NUMBER_OF_PEOPLE
        defaultRecipeShouldBeFound("numberOfPeople.equals=" + DEFAULT_NUMBER_OF_PEOPLE);

        // Get all the recipeList where numberOfPeople equals to UPDATED_NUMBER_OF_PEOPLE
        defaultRecipeShouldNotBeFound("numberOfPeople.equals=" + UPDATED_NUMBER_OF_PEOPLE);
    }

    @Test
    @Transactional
    void getAllRecipesByNumberOfPeopleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where numberOfPeople not equals to DEFAULT_NUMBER_OF_PEOPLE
        defaultRecipeShouldNotBeFound("numberOfPeople.notEquals=" + DEFAULT_NUMBER_OF_PEOPLE);

        // Get all the recipeList where numberOfPeople not equals to UPDATED_NUMBER_OF_PEOPLE
        defaultRecipeShouldBeFound("numberOfPeople.notEquals=" + UPDATED_NUMBER_OF_PEOPLE);
    }

    @Test
    @Transactional
    void getAllRecipesByNumberOfPeopleIsInShouldWork() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where numberOfPeople in DEFAULT_NUMBER_OF_PEOPLE or UPDATED_NUMBER_OF_PEOPLE
        defaultRecipeShouldBeFound("numberOfPeople.in=" + DEFAULT_NUMBER_OF_PEOPLE + "," + UPDATED_NUMBER_OF_PEOPLE);

        // Get all the recipeList where numberOfPeople equals to UPDATED_NUMBER_OF_PEOPLE
        defaultRecipeShouldNotBeFound("numberOfPeople.in=" + UPDATED_NUMBER_OF_PEOPLE);
    }

    @Test
    @Transactional
    void getAllRecipesByNumberOfPeopleIsNullOrNotNull() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where numberOfPeople is not null
        defaultRecipeShouldBeFound("numberOfPeople.specified=true");

        // Get all the recipeList where numberOfPeople is null
        defaultRecipeShouldNotBeFound("numberOfPeople.specified=false");
    }

    @Test
    @Transactional
    void getAllRecipesByNumberOfPeopleIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where numberOfPeople is greater than or equal to DEFAULT_NUMBER_OF_PEOPLE
        defaultRecipeShouldBeFound("numberOfPeople.greaterThanOrEqual=" + DEFAULT_NUMBER_OF_PEOPLE);

        // Get all the recipeList where numberOfPeople is greater than or equal to UPDATED_NUMBER_OF_PEOPLE
        defaultRecipeShouldNotBeFound("numberOfPeople.greaterThanOrEqual=" + UPDATED_NUMBER_OF_PEOPLE);
    }

    @Test
    @Transactional
    void getAllRecipesByNumberOfPeopleIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where numberOfPeople is less than or equal to DEFAULT_NUMBER_OF_PEOPLE
        defaultRecipeShouldBeFound("numberOfPeople.lessThanOrEqual=" + DEFAULT_NUMBER_OF_PEOPLE);

        // Get all the recipeList where numberOfPeople is less than or equal to SMALLER_NUMBER_OF_PEOPLE
        defaultRecipeShouldNotBeFound("numberOfPeople.lessThanOrEqual=" + SMALLER_NUMBER_OF_PEOPLE);
    }

    @Test
    @Transactional
    void getAllRecipesByNumberOfPeopleIsLessThanSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where numberOfPeople is less than DEFAULT_NUMBER_OF_PEOPLE
        defaultRecipeShouldNotBeFound("numberOfPeople.lessThan=" + DEFAULT_NUMBER_OF_PEOPLE);

        // Get all the recipeList where numberOfPeople is less than UPDATED_NUMBER_OF_PEOPLE
        defaultRecipeShouldBeFound("numberOfPeople.lessThan=" + UPDATED_NUMBER_OF_PEOPLE);
    }

    @Test
    @Transactional
    void getAllRecipesByNumberOfPeopleIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where numberOfPeople is greater than DEFAULT_NUMBER_OF_PEOPLE
        defaultRecipeShouldNotBeFound("numberOfPeople.greaterThan=" + DEFAULT_NUMBER_OF_PEOPLE);

        // Get all the recipeList where numberOfPeople is greater than SMALLER_NUMBER_OF_PEOPLE
        defaultRecipeShouldBeFound("numberOfPeople.greaterThan=" + SMALLER_NUMBER_OF_PEOPLE);
    }

    @Test
    @Transactional
    void getAllRecipesByRecipeTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where recipeType equals to DEFAULT_RECIPE_TYPE
        defaultRecipeShouldBeFound("recipeType.equals=" + DEFAULT_RECIPE_TYPE);

        // Get all the recipeList where recipeType equals to UPDATED_RECIPE_TYPE
        defaultRecipeShouldNotBeFound("recipeType.equals=" + UPDATED_RECIPE_TYPE);
    }

    @Test
    @Transactional
    void getAllRecipesByRecipeTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where recipeType not equals to DEFAULT_RECIPE_TYPE
        defaultRecipeShouldNotBeFound("recipeType.notEquals=" + DEFAULT_RECIPE_TYPE);

        // Get all the recipeList where recipeType not equals to UPDATED_RECIPE_TYPE
        defaultRecipeShouldBeFound("recipeType.notEquals=" + UPDATED_RECIPE_TYPE);
    }

    @Test
    @Transactional
    void getAllRecipesByRecipeTypeIsInShouldWork() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where recipeType in DEFAULT_RECIPE_TYPE or UPDATED_RECIPE_TYPE
        defaultRecipeShouldBeFound("recipeType.in=" + DEFAULT_RECIPE_TYPE + "," + UPDATED_RECIPE_TYPE);

        // Get all the recipeList where recipeType equals to UPDATED_RECIPE_TYPE
        defaultRecipeShouldNotBeFound("recipeType.in=" + UPDATED_RECIPE_TYPE);
    }

    @Test
    @Transactional
    void getAllRecipesByRecipeTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where recipeType is not null
        defaultRecipeShouldBeFound("recipeType.specified=true");

        // Get all the recipeList where recipeType is null
        defaultRecipeShouldNotBeFound("recipeType.specified=false");
    }

    @Test
    @Transactional
    void getAllRecipesByRecipeURLIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where recipeURL equals to DEFAULT_RECIPE_URL
        defaultRecipeShouldBeFound("recipeURL.equals=" + DEFAULT_RECIPE_URL);

        // Get all the recipeList where recipeURL equals to UPDATED_RECIPE_URL
        defaultRecipeShouldNotBeFound("recipeURL.equals=" + UPDATED_RECIPE_URL);
    }

    @Test
    @Transactional
    void getAllRecipesByRecipeURLIsNotEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where recipeURL not equals to DEFAULT_RECIPE_URL
        defaultRecipeShouldNotBeFound("recipeURL.notEquals=" + DEFAULT_RECIPE_URL);

        // Get all the recipeList where recipeURL not equals to UPDATED_RECIPE_URL
        defaultRecipeShouldBeFound("recipeURL.notEquals=" + UPDATED_RECIPE_URL);
    }

    @Test
    @Transactional
    void getAllRecipesByRecipeURLIsInShouldWork() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where recipeURL in DEFAULT_RECIPE_URL or UPDATED_RECIPE_URL
        defaultRecipeShouldBeFound("recipeURL.in=" + DEFAULT_RECIPE_URL + "," + UPDATED_RECIPE_URL);

        // Get all the recipeList where recipeURL equals to UPDATED_RECIPE_URL
        defaultRecipeShouldNotBeFound("recipeURL.in=" + UPDATED_RECIPE_URL);
    }

    @Test
    @Transactional
    void getAllRecipesByRecipeURLIsNullOrNotNull() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where recipeURL is not null
        defaultRecipeShouldBeFound("recipeURL.specified=true");

        // Get all the recipeList where recipeURL is null
        defaultRecipeShouldNotBeFound("recipeURL.specified=false");
    }

    @Test
    @Transactional
    void getAllRecipesByRecipeURLContainsSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where recipeURL contains DEFAULT_RECIPE_URL
        defaultRecipeShouldBeFound("recipeURL.contains=" + DEFAULT_RECIPE_URL);

        // Get all the recipeList where recipeURL contains UPDATED_RECIPE_URL
        defaultRecipeShouldNotBeFound("recipeURL.contains=" + UPDATED_RECIPE_URL);
    }

    @Test
    @Transactional
    void getAllRecipesByRecipeURLNotContainsSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where recipeURL does not contain DEFAULT_RECIPE_URL
        defaultRecipeShouldNotBeFound("recipeURL.doesNotContain=" + DEFAULT_RECIPE_URL);

        // Get all the recipeList where recipeURL does not contain UPDATED_RECIPE_URL
        defaultRecipeShouldBeFound("recipeURL.doesNotContain=" + UPDATED_RECIPE_URL);
    }

    @Test
    @Transactional
    void getAllRecipesByShortStepsIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where shortSteps equals to DEFAULT_SHORT_STEPS
        defaultRecipeShouldBeFound("shortSteps.equals=" + DEFAULT_SHORT_STEPS);

        // Get all the recipeList where shortSteps equals to UPDATED_SHORT_STEPS
        defaultRecipeShouldNotBeFound("shortSteps.equals=" + UPDATED_SHORT_STEPS);
    }

    @Test
    @Transactional
    void getAllRecipesByShortStepsIsNotEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where shortSteps not equals to DEFAULT_SHORT_STEPS
        defaultRecipeShouldNotBeFound("shortSteps.notEquals=" + DEFAULT_SHORT_STEPS);

        // Get all the recipeList where shortSteps not equals to UPDATED_SHORT_STEPS
        defaultRecipeShouldBeFound("shortSteps.notEquals=" + UPDATED_SHORT_STEPS);
    }

    @Test
    @Transactional
    void getAllRecipesByShortStepsIsInShouldWork() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where shortSteps in DEFAULT_SHORT_STEPS or UPDATED_SHORT_STEPS
        defaultRecipeShouldBeFound("shortSteps.in=" + DEFAULT_SHORT_STEPS + "," + UPDATED_SHORT_STEPS);

        // Get all the recipeList where shortSteps equals to UPDATED_SHORT_STEPS
        defaultRecipeShouldNotBeFound("shortSteps.in=" + UPDATED_SHORT_STEPS);
    }

    @Test
    @Transactional
    void getAllRecipesByShortStepsIsNullOrNotNull() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where shortSteps is not null
        defaultRecipeShouldBeFound("shortSteps.specified=true");

        // Get all the recipeList where shortSteps is null
        defaultRecipeShouldNotBeFound("shortSteps.specified=false");
    }

    @Test
    @Transactional
    void getAllRecipesByShortStepsContainsSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where shortSteps contains DEFAULT_SHORT_STEPS
        defaultRecipeShouldBeFound("shortSteps.contains=" + DEFAULT_SHORT_STEPS);

        // Get all the recipeList where shortSteps contains UPDATED_SHORT_STEPS
        defaultRecipeShouldNotBeFound("shortSteps.contains=" + UPDATED_SHORT_STEPS);
    }

    @Test
    @Transactional
    void getAllRecipesByShortStepsNotContainsSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where shortSteps does not contain DEFAULT_SHORT_STEPS
        defaultRecipeShouldNotBeFound("shortSteps.doesNotContain=" + DEFAULT_SHORT_STEPS);

        // Get all the recipeList where shortSteps does not contain UPDATED_SHORT_STEPS
        defaultRecipeShouldBeFound("shortSteps.doesNotContain=" + UPDATED_SHORT_STEPS);
    }

    @Test
    @Transactional
    void getAllRecipesByIsShortDescRecipeIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where isShortDescRecipe equals to DEFAULT_IS_SHORT_DESC_RECIPE
        defaultRecipeShouldBeFound("isShortDescRecipe.equals=" + DEFAULT_IS_SHORT_DESC_RECIPE);

        // Get all the recipeList where isShortDescRecipe equals to UPDATED_IS_SHORT_DESC_RECIPE
        defaultRecipeShouldNotBeFound("isShortDescRecipe.equals=" + UPDATED_IS_SHORT_DESC_RECIPE);
    }

    @Test
    @Transactional
    void getAllRecipesByIsShortDescRecipeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where isShortDescRecipe not equals to DEFAULT_IS_SHORT_DESC_RECIPE
        defaultRecipeShouldNotBeFound("isShortDescRecipe.notEquals=" + DEFAULT_IS_SHORT_DESC_RECIPE);

        // Get all the recipeList where isShortDescRecipe not equals to UPDATED_IS_SHORT_DESC_RECIPE
        defaultRecipeShouldBeFound("isShortDescRecipe.notEquals=" + UPDATED_IS_SHORT_DESC_RECIPE);
    }

    @Test
    @Transactional
    void getAllRecipesByIsShortDescRecipeIsInShouldWork() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where isShortDescRecipe in DEFAULT_IS_SHORT_DESC_RECIPE or UPDATED_IS_SHORT_DESC_RECIPE
        defaultRecipeShouldBeFound("isShortDescRecipe.in=" + DEFAULT_IS_SHORT_DESC_RECIPE + "," + UPDATED_IS_SHORT_DESC_RECIPE);

        // Get all the recipeList where isShortDescRecipe equals to UPDATED_IS_SHORT_DESC_RECIPE
        defaultRecipeShouldNotBeFound("isShortDescRecipe.in=" + UPDATED_IS_SHORT_DESC_RECIPE);
    }

    @Test
    @Transactional
    void getAllRecipesByIsShortDescRecipeIsNullOrNotNull() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        // Get all the recipeList where isShortDescRecipe is not null
        defaultRecipeShouldBeFound("isShortDescRecipe.specified=true");

        // Get all the recipeList where isShortDescRecipe is null
        defaultRecipeShouldNotBeFound("isShortDescRecipe.specified=false");
    }

    @Test
    @Transactional
    void getAllRecipesByTagIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);
        Tag tag;
        if (TestUtil.findAll(em, Tag.class).isEmpty()) {
            tag = TagResourceIT.createEntity(em);
            em.persist(tag);
            em.flush();
        } else {
            tag = TestUtil.findAll(em, Tag.class).get(0);
        }
        em.persist(tag);
        em.flush();
        recipe.addTag(tag);
        recipeRepository.saveAndFlush(recipe);
        Long tagId = tag.getId();

        // Get all the recipeList where tag equals to tagId
        defaultRecipeShouldBeFound("tagId.equals=" + tagId);

        // Get all the recipeList where tag equals to (tagId + 1)
        defaultRecipeShouldNotBeFound("tagId.equals=" + (tagId + 1));
    }

    @Test
    @Transactional
    void getAllRecipesByIngredientIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);
        Ingredient ingredient;
        if (TestUtil.findAll(em, Ingredient.class).isEmpty()) {
            ingredient = IngredientResourceIT.createEntity(em);
            em.persist(ingredient);
            em.flush();
        } else {
            ingredient = TestUtil.findAll(em, Ingredient.class).get(0);
        }
        em.persist(ingredient);
        em.flush();
        recipe.addIngredient(ingredient);
        recipeRepository.saveAndFlush(recipe);
        Long ingredientId = ingredient.getId();

        // Get all the recipeList where ingredient equals to ingredientId
        defaultRecipeShouldBeFound("ingredientId.equals=" + ingredientId);

        // Get all the recipeList where ingredient equals to (ingredientId + 1)
        defaultRecipeShouldNotBeFound("ingredientId.equals=" + (ingredientId + 1));
    }

    @Test
    @Transactional
    void getAllRecipesByRecipePictureIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);
        RecipePicture recipePicture;
        if (TestUtil.findAll(em, RecipePicture.class).isEmpty()) {
            recipePicture = RecipePictureResourceIT.createEntity(em);
            em.persist(recipePicture);
            em.flush();
        } else {
            recipePicture = TestUtil.findAll(em, RecipePicture.class).get(0);
        }
        em.persist(recipePicture);
        em.flush();
        recipe.addRecipePicture(recipePicture);
        recipeRepository.saveAndFlush(recipe);
        Long recipePictureId = recipePicture.getId();

        // Get all the recipeList where recipePicture equals to recipePictureId
        defaultRecipeShouldBeFound("recipePictureId.equals=" + recipePictureId);

        // Get all the recipeList where recipePicture equals to (recipePictureId + 1)
        defaultRecipeShouldNotBeFound("recipePictureId.equals=" + (recipePictureId + 1));
    }

    @Test
    @Transactional
    void getAllRecipesByRecipeStepIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);
        RecipeStep recipeStep;
        if (TestUtil.findAll(em, RecipeStep.class).isEmpty()) {
            recipeStep = RecipeStepResourceIT.createEntity(em);
            em.persist(recipeStep);
            em.flush();
        } else {
            recipeStep = TestUtil.findAll(em, RecipeStep.class).get(0);
        }
        em.persist(recipeStep);
        em.flush();
        recipe.addRecipeStep(recipeStep);
        recipeRepository.saveAndFlush(recipe);
        Long recipeStepId = recipeStep.getId();

        // Get all the recipeList where recipeStep equals to recipeStepId
        defaultRecipeShouldBeFound("recipeStepId.equals=" + recipeStepId);

        // Get all the recipeList where recipeStep equals to (recipeStepId + 1)
        defaultRecipeShouldNotBeFound("recipeStepId.equals=" + (recipeStepId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRecipeShouldBeFound(String filter) throws Exception {
        restRecipeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recipe.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].preparationDuration").value(hasItem(DEFAULT_PREPARATION_DURATION)))
            .andExpect(jsonPath("$.[*].restDuraction").value(hasItem(DEFAULT_REST_DURACTION)))
            .andExpect(jsonPath("$.[*].cookingDuration").value(hasItem(DEFAULT_COOKING_DURATION)))
            .andExpect(jsonPath("$.[*].winterPreferrency").value(hasItem(DEFAULT_WINTER_PREFERRENCY.toString())))
            .andExpect(jsonPath("$.[*].springPreferrency").value(hasItem(DEFAULT_SPRING_PREFERRENCY.toString())))
            .andExpect(jsonPath("$.[*].summerPreferrency").value(hasItem(DEFAULT_SUMMER_PREFERRENCY.toString())))
            .andExpect(jsonPath("$.[*].autumnPreferrency").value(hasItem(DEFAULT_AUTUMN_PREFERRENCY.toString())))
            .andExpect(jsonPath("$.[*].numberOfPeople").value(hasItem(DEFAULT_NUMBER_OF_PEOPLE)))
            .andExpect(jsonPath("$.[*].recipeType").value(hasItem(DEFAULT_RECIPE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].recipeURL").value(hasItem(DEFAULT_RECIPE_URL)))
            .andExpect(jsonPath("$.[*].shortSteps").value(hasItem(DEFAULT_SHORT_STEPS)))
            .andExpect(jsonPath("$.[*].isShortDescRecipe").value(hasItem(DEFAULT_IS_SHORT_DESC_RECIPE.booleanValue())));

        // Check, that the count call also returns 1
        restRecipeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRecipeShouldNotBeFound(String filter) throws Exception {
        restRecipeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRecipeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingRecipe() throws Exception {
        // Get the recipe
        restRecipeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewRecipe() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        int databaseSizeBeforeUpdate = recipeRepository.findAll().size();

        // Update the recipe
        Recipe updatedRecipe = recipeRepository.findById(recipe.getId()).get();
        // Disconnect from session so that the updates on updatedRecipe are not directly saved in db
        em.detach(updatedRecipe);
        updatedRecipe
            .name(UPDATED_NAME)
            .creationDate(UPDATED_CREATION_DATE)
            .description(UPDATED_DESCRIPTION)
            .preparationDuration(UPDATED_PREPARATION_DURATION)
            .restDuraction(UPDATED_REST_DURACTION)
            .cookingDuration(UPDATED_COOKING_DURATION)
            .winterPreferrency(UPDATED_WINTER_PREFERRENCY)
            .springPreferrency(UPDATED_SPRING_PREFERRENCY)
            .summerPreferrency(UPDATED_SUMMER_PREFERRENCY)
            .autumnPreferrency(UPDATED_AUTUMN_PREFERRENCY)
            .numberOfPeople(UPDATED_NUMBER_OF_PEOPLE)
            .recipeType(UPDATED_RECIPE_TYPE)
            .recipeURL(UPDATED_RECIPE_URL)
            .shortSteps(UPDATED_SHORT_STEPS)
            .isShortDescRecipe(UPDATED_IS_SHORT_DESC_RECIPE);
        RecipeDTO recipeDTO = recipeMapper.toDto(updatedRecipe);

        restRecipeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, recipeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(recipeDTO))
            )
            .andExpect(status().isOk());

        // Validate the Recipe in the database
        List<Recipe> recipeList = recipeRepository.findAll();
        assertThat(recipeList).hasSize(databaseSizeBeforeUpdate);
        Recipe testRecipe = recipeList.get(recipeList.size() - 1);
        assertThat(testRecipe.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testRecipe.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testRecipe.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testRecipe.getPreparationDuration()).isEqualTo(UPDATED_PREPARATION_DURATION);
        assertThat(testRecipe.getRestDuraction()).isEqualTo(UPDATED_REST_DURACTION);
        assertThat(testRecipe.getCookingDuration()).isEqualTo(UPDATED_COOKING_DURATION);
        assertThat(testRecipe.getWinterPreferrency()).isEqualTo(UPDATED_WINTER_PREFERRENCY);
        assertThat(testRecipe.getSpringPreferrency()).isEqualTo(UPDATED_SPRING_PREFERRENCY);
        assertThat(testRecipe.getSummerPreferrency()).isEqualTo(UPDATED_SUMMER_PREFERRENCY);
        assertThat(testRecipe.getAutumnPreferrency()).isEqualTo(UPDATED_AUTUMN_PREFERRENCY);
        assertThat(testRecipe.getNumberOfPeople()).isEqualTo(UPDATED_NUMBER_OF_PEOPLE);
        assertThat(testRecipe.getRecipeType()).isEqualTo(UPDATED_RECIPE_TYPE);
        assertThat(testRecipe.getRecipeURL()).isEqualTo(UPDATED_RECIPE_URL);
        assertThat(testRecipe.getShortSteps()).isEqualTo(UPDATED_SHORT_STEPS);
        assertThat(testRecipe.getIsShortDescRecipe()).isEqualTo(UPDATED_IS_SHORT_DESC_RECIPE);

        // Validate the Recipe in Elasticsearch
        verify(mockRecipeSearchRepository).save(testRecipe);
    }

    @Test
    @Transactional
    void putNonExistingRecipe() throws Exception {
        int databaseSizeBeforeUpdate = recipeRepository.findAll().size();
        recipe.setId(count.incrementAndGet());

        // Create the Recipe
        RecipeDTO recipeDTO = recipeMapper.toDto(recipe);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRecipeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, recipeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(recipeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Recipe in the database
        List<Recipe> recipeList = recipeRepository.findAll();
        assertThat(recipeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Recipe in Elasticsearch
        verify(mockRecipeSearchRepository, times(0)).save(recipe);
    }

    @Test
    @Transactional
    void putWithIdMismatchRecipe() throws Exception {
        int databaseSizeBeforeUpdate = recipeRepository.findAll().size();
        recipe.setId(count.incrementAndGet());

        // Create the Recipe
        RecipeDTO recipeDTO = recipeMapper.toDto(recipe);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRecipeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(recipeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Recipe in the database
        List<Recipe> recipeList = recipeRepository.findAll();
        assertThat(recipeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Recipe in Elasticsearch
        verify(mockRecipeSearchRepository, times(0)).save(recipe);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamRecipe() throws Exception {
        int databaseSizeBeforeUpdate = recipeRepository.findAll().size();
        recipe.setId(count.incrementAndGet());

        // Create the Recipe
        RecipeDTO recipeDTO = recipeMapper.toDto(recipe);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRecipeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(recipeDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Recipe in the database
        List<Recipe> recipeList = recipeRepository.findAll();
        assertThat(recipeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Recipe in Elasticsearch
        verify(mockRecipeSearchRepository, times(0)).save(recipe);
    }

    @Test
    @Transactional
    void partialUpdateRecipeWithPatch() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        int databaseSizeBeforeUpdate = recipeRepository.findAll().size();

        // Update the recipe using partial update
        Recipe partialUpdatedRecipe = new Recipe();
        partialUpdatedRecipe.setId(recipe.getId());

        partialUpdatedRecipe
            .name(UPDATED_NAME)
            .creationDate(UPDATED_CREATION_DATE)
            .autumnPreferrency(UPDATED_AUTUMN_PREFERRENCY)
            .numberOfPeople(UPDATED_NUMBER_OF_PEOPLE)
            .shortSteps(UPDATED_SHORT_STEPS);

        restRecipeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRecipe.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRecipe))
            )
            .andExpect(status().isOk());

        // Validate the Recipe in the database
        List<Recipe> recipeList = recipeRepository.findAll();
        assertThat(recipeList).hasSize(databaseSizeBeforeUpdate);
        Recipe testRecipe = recipeList.get(recipeList.size() - 1);
        assertThat(testRecipe.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testRecipe.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testRecipe.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testRecipe.getPreparationDuration()).isEqualTo(DEFAULT_PREPARATION_DURATION);
        assertThat(testRecipe.getRestDuraction()).isEqualTo(DEFAULT_REST_DURACTION);
        assertThat(testRecipe.getCookingDuration()).isEqualTo(DEFAULT_COOKING_DURATION);
        assertThat(testRecipe.getWinterPreferrency()).isEqualTo(DEFAULT_WINTER_PREFERRENCY);
        assertThat(testRecipe.getSpringPreferrency()).isEqualTo(DEFAULT_SPRING_PREFERRENCY);
        assertThat(testRecipe.getSummerPreferrency()).isEqualTo(DEFAULT_SUMMER_PREFERRENCY);
        assertThat(testRecipe.getAutumnPreferrency()).isEqualTo(UPDATED_AUTUMN_PREFERRENCY);
        assertThat(testRecipe.getNumberOfPeople()).isEqualTo(UPDATED_NUMBER_OF_PEOPLE);
        assertThat(testRecipe.getRecipeType()).isEqualTo(DEFAULT_RECIPE_TYPE);
        assertThat(testRecipe.getRecipeURL()).isEqualTo(DEFAULT_RECIPE_URL);
        assertThat(testRecipe.getShortSteps()).isEqualTo(UPDATED_SHORT_STEPS);
        assertThat(testRecipe.getIsShortDescRecipe()).isEqualTo(DEFAULT_IS_SHORT_DESC_RECIPE);
    }

    @Test
    @Transactional
    void fullUpdateRecipeWithPatch() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        int databaseSizeBeforeUpdate = recipeRepository.findAll().size();

        // Update the recipe using partial update
        Recipe partialUpdatedRecipe = new Recipe();
        partialUpdatedRecipe.setId(recipe.getId());

        partialUpdatedRecipe
            .name(UPDATED_NAME)
            .creationDate(UPDATED_CREATION_DATE)
            .description(UPDATED_DESCRIPTION)
            .preparationDuration(UPDATED_PREPARATION_DURATION)
            .restDuraction(UPDATED_REST_DURACTION)
            .cookingDuration(UPDATED_COOKING_DURATION)
            .winterPreferrency(UPDATED_WINTER_PREFERRENCY)
            .springPreferrency(UPDATED_SPRING_PREFERRENCY)
            .summerPreferrency(UPDATED_SUMMER_PREFERRENCY)
            .autumnPreferrency(UPDATED_AUTUMN_PREFERRENCY)
            .numberOfPeople(UPDATED_NUMBER_OF_PEOPLE)
            .recipeType(UPDATED_RECIPE_TYPE)
            .recipeURL(UPDATED_RECIPE_URL)
            .shortSteps(UPDATED_SHORT_STEPS)
            .isShortDescRecipe(UPDATED_IS_SHORT_DESC_RECIPE);

        restRecipeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRecipe.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRecipe))
            )
            .andExpect(status().isOk());

        // Validate the Recipe in the database
        List<Recipe> recipeList = recipeRepository.findAll();
        assertThat(recipeList).hasSize(databaseSizeBeforeUpdate);
        Recipe testRecipe = recipeList.get(recipeList.size() - 1);
        assertThat(testRecipe.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testRecipe.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testRecipe.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testRecipe.getPreparationDuration()).isEqualTo(UPDATED_PREPARATION_DURATION);
        assertThat(testRecipe.getRestDuraction()).isEqualTo(UPDATED_REST_DURACTION);
        assertThat(testRecipe.getCookingDuration()).isEqualTo(UPDATED_COOKING_DURATION);
        assertThat(testRecipe.getWinterPreferrency()).isEqualTo(UPDATED_WINTER_PREFERRENCY);
        assertThat(testRecipe.getSpringPreferrency()).isEqualTo(UPDATED_SPRING_PREFERRENCY);
        assertThat(testRecipe.getSummerPreferrency()).isEqualTo(UPDATED_SUMMER_PREFERRENCY);
        assertThat(testRecipe.getAutumnPreferrency()).isEqualTo(UPDATED_AUTUMN_PREFERRENCY);
        assertThat(testRecipe.getNumberOfPeople()).isEqualTo(UPDATED_NUMBER_OF_PEOPLE);
        assertThat(testRecipe.getRecipeType()).isEqualTo(UPDATED_RECIPE_TYPE);
        assertThat(testRecipe.getRecipeURL()).isEqualTo(UPDATED_RECIPE_URL);
        assertThat(testRecipe.getShortSteps()).isEqualTo(UPDATED_SHORT_STEPS);
        assertThat(testRecipe.getIsShortDescRecipe()).isEqualTo(UPDATED_IS_SHORT_DESC_RECIPE);
    }

    @Test
    @Transactional
    void patchNonExistingRecipe() throws Exception {
        int databaseSizeBeforeUpdate = recipeRepository.findAll().size();
        recipe.setId(count.incrementAndGet());

        // Create the Recipe
        RecipeDTO recipeDTO = recipeMapper.toDto(recipe);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRecipeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, recipeDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(recipeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Recipe in the database
        List<Recipe> recipeList = recipeRepository.findAll();
        assertThat(recipeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Recipe in Elasticsearch
        verify(mockRecipeSearchRepository, times(0)).save(recipe);
    }

    @Test
    @Transactional
    void patchWithIdMismatchRecipe() throws Exception {
        int databaseSizeBeforeUpdate = recipeRepository.findAll().size();
        recipe.setId(count.incrementAndGet());

        // Create the Recipe
        RecipeDTO recipeDTO = recipeMapper.toDto(recipe);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRecipeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(recipeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Recipe in the database
        List<Recipe> recipeList = recipeRepository.findAll();
        assertThat(recipeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Recipe in Elasticsearch
        verify(mockRecipeSearchRepository, times(0)).save(recipe);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamRecipe() throws Exception {
        int databaseSizeBeforeUpdate = recipeRepository.findAll().size();
        recipe.setId(count.incrementAndGet());

        // Create the Recipe
        RecipeDTO recipeDTO = recipeMapper.toDto(recipe);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRecipeMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(recipeDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Recipe in the database
        List<Recipe> recipeList = recipeRepository.findAll();
        assertThat(recipeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Recipe in Elasticsearch
        verify(mockRecipeSearchRepository, times(0)).save(recipe);
    }

    @Test
    @Transactional
    void deleteRecipe() throws Exception {
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);

        int databaseSizeBeforeDelete = recipeRepository.findAll().size();

        // Delete the recipe
        restRecipeMockMvc
            .perform(delete(ENTITY_API_URL_ID, recipe.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Recipe> recipeList = recipeRepository.findAll();
        assertThat(recipeList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Recipe in Elasticsearch
        verify(mockRecipeSearchRepository, times(1)).deleteById(recipe.getId());
    }

    @Test
    @Transactional
    void searchRecipe() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        recipeRepository.saveAndFlush(recipe);
        when(mockRecipeSearchRepository.search("id:" + recipe.getId(), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(recipe), PageRequest.of(0, 1), 1));

        // Search the recipe
        restRecipeMockMvc
            .perform(get(ENTITY_SEARCH_API_URL + "?query=id:" + recipe.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recipe.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].preparationDuration").value(hasItem(DEFAULT_PREPARATION_DURATION)))
            .andExpect(jsonPath("$.[*].restDuraction").value(hasItem(DEFAULT_REST_DURACTION)))
            .andExpect(jsonPath("$.[*].cookingDuration").value(hasItem(DEFAULT_COOKING_DURATION)))
            .andExpect(jsonPath("$.[*].winterPreferrency").value(hasItem(DEFAULT_WINTER_PREFERRENCY.toString())))
            .andExpect(jsonPath("$.[*].springPreferrency").value(hasItem(DEFAULT_SPRING_PREFERRENCY.toString())))
            .andExpect(jsonPath("$.[*].summerPreferrency").value(hasItem(DEFAULT_SUMMER_PREFERRENCY.toString())))
            .andExpect(jsonPath("$.[*].autumnPreferrency").value(hasItem(DEFAULT_AUTUMN_PREFERRENCY.toString())))
            .andExpect(jsonPath("$.[*].numberOfPeople").value(hasItem(DEFAULT_NUMBER_OF_PEOPLE)))
            .andExpect(jsonPath("$.[*].recipeType").value(hasItem(DEFAULT_RECIPE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].recipeURL").value(hasItem(DEFAULT_RECIPE_URL)))
            .andExpect(jsonPath("$.[*].shortSteps").value(hasItem(DEFAULT_SHORT_STEPS)))
            .andExpect(jsonPath("$.[*].isShortDescRecipe").value(hasItem(DEFAULT_IS_SHORT_DESC_RECIPE.booleanValue())));
    }
}

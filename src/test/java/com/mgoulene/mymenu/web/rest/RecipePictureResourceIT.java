package com.mgoulene.mymenu.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mgoulene.mymenu.IntegrationTest;
import com.mgoulene.mymenu.domain.Recipe;
import com.mgoulene.mymenu.domain.RecipePicture;
import com.mgoulene.mymenu.repository.RecipePictureRepository;
import com.mgoulene.mymenu.repository.search.RecipePictureSearchRepository;
import com.mgoulene.mymenu.service.criteria.RecipePictureCriteria;
import com.mgoulene.mymenu.service.dto.RecipePictureDTO;
import com.mgoulene.mymenu.service.mapper.RecipePictureMapper;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link RecipePictureResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class RecipePictureResourceIT {

    private static final byte[] DEFAULT_PICTURE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_PICTURE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_PICTURE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PICTURE_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final Integer DEFAULT_ORDER = 1;
    private static final Integer UPDATED_ORDER = 2;
    private static final Integer SMALLER_ORDER = 1 - 1;

    private static final String ENTITY_API_URL = "/api/recipe-pictures";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/recipe-pictures";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private RecipePictureRepository recipePictureRepository;

    @Autowired
    private RecipePictureMapper recipePictureMapper;

    /**
     * This repository is mocked in the com.mgoulene.mymenu.repository.search test package.
     *
     * @see com.mgoulene.mymenu.repository.search.RecipePictureSearchRepositoryMockConfiguration
     */
    @Autowired
    private RecipePictureSearchRepository mockRecipePictureSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRecipePictureMockMvc;

    private RecipePicture recipePicture;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecipePicture createEntity(EntityManager em) {
        RecipePicture recipePicture = new RecipePicture()
            .picture(DEFAULT_PICTURE)
            .pictureContentType(DEFAULT_PICTURE_CONTENT_TYPE)
            .label(DEFAULT_LABEL)
            .order(DEFAULT_ORDER);
        return recipePicture;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecipePicture createUpdatedEntity(EntityManager em) {
        RecipePicture recipePicture = new RecipePicture()
            .picture(UPDATED_PICTURE)
            .pictureContentType(UPDATED_PICTURE_CONTENT_TYPE)
            .label(UPDATED_LABEL)
            .order(UPDATED_ORDER);
        return recipePicture;
    }

    @BeforeEach
    public void initTest() {
        recipePicture = createEntity(em);
    }

    @Test
    @Transactional
    void createRecipePicture() throws Exception {
        int databaseSizeBeforeCreate = recipePictureRepository.findAll().size();
        // Create the RecipePicture
        RecipePictureDTO recipePictureDTO = recipePictureMapper.toDto(recipePicture);
        restRecipePictureMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(recipePictureDTO))
            )
            .andExpect(status().isCreated());

        // Validate the RecipePicture in the database
        List<RecipePicture> recipePictureList = recipePictureRepository.findAll();
        assertThat(recipePictureList).hasSize(databaseSizeBeforeCreate + 1);
        RecipePicture testRecipePicture = recipePictureList.get(recipePictureList.size() - 1);
        assertThat(testRecipePicture.getPicture()).isEqualTo(DEFAULT_PICTURE);
        assertThat(testRecipePicture.getPictureContentType()).isEqualTo(DEFAULT_PICTURE_CONTENT_TYPE);
        assertThat(testRecipePicture.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testRecipePicture.getOrder()).isEqualTo(DEFAULT_ORDER);

        // Validate the RecipePicture in Elasticsearch
        verify(mockRecipePictureSearchRepository, times(1)).save(testRecipePicture);
    }

    @Test
    @Transactional
    void createRecipePictureWithExistingId() throws Exception {
        // Create the RecipePicture with an existing ID
        recipePicture.setId(1L);
        RecipePictureDTO recipePictureDTO = recipePictureMapper.toDto(recipePicture);

        int databaseSizeBeforeCreate = recipePictureRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restRecipePictureMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(recipePictureDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RecipePicture in the database
        List<RecipePicture> recipePictureList = recipePictureRepository.findAll();
        assertThat(recipePictureList).hasSize(databaseSizeBeforeCreate);

        // Validate the RecipePicture in Elasticsearch
        verify(mockRecipePictureSearchRepository, times(0)).save(recipePicture);
    }

    @Test
    @Transactional
    void checkOrderIsRequired() throws Exception {
        int databaseSizeBeforeTest = recipePictureRepository.findAll().size();
        // set the field null
        recipePicture.setOrder(null);

        // Create the RecipePicture, which fails.
        RecipePictureDTO recipePictureDTO = recipePictureMapper.toDto(recipePicture);

        restRecipePictureMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(recipePictureDTO))
            )
            .andExpect(status().isBadRequest());

        List<RecipePicture> recipePictureList = recipePictureRepository.findAll();
        assertThat(recipePictureList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllRecipePictures() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        // Get all the recipePictureList
        restRecipePictureMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recipePicture.getId().intValue())))
            .andExpect(jsonPath("$.[*].pictureContentType").value(hasItem(DEFAULT_PICTURE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].picture").value(hasItem(Base64Utils.encodeToString(DEFAULT_PICTURE))))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER)));
    }

    @Test
    @Transactional
    void getRecipePicture() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        // Get the recipePicture
        restRecipePictureMockMvc
            .perform(get(ENTITY_API_URL_ID, recipePicture.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(recipePicture.getId().intValue()))
            .andExpect(jsonPath("$.pictureContentType").value(DEFAULT_PICTURE_CONTENT_TYPE))
            .andExpect(jsonPath("$.picture").value(Base64Utils.encodeToString(DEFAULT_PICTURE)))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.order").value(DEFAULT_ORDER));
    }

    @Test
    @Transactional
    void getRecipePicturesByIdFiltering() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        Long id = recipePicture.getId();

        defaultRecipePictureShouldBeFound("id.equals=" + id);
        defaultRecipePictureShouldNotBeFound("id.notEquals=" + id);

        defaultRecipePictureShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultRecipePictureShouldNotBeFound("id.greaterThan=" + id);

        defaultRecipePictureShouldBeFound("id.lessThanOrEqual=" + id);
        defaultRecipePictureShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllRecipePicturesByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        // Get all the recipePictureList where label equals to DEFAULT_LABEL
        defaultRecipePictureShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the recipePictureList where label equals to UPDATED_LABEL
        defaultRecipePictureShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    void getAllRecipePicturesByLabelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        // Get all the recipePictureList where label not equals to DEFAULT_LABEL
        defaultRecipePictureShouldNotBeFound("label.notEquals=" + DEFAULT_LABEL);

        // Get all the recipePictureList where label not equals to UPDATED_LABEL
        defaultRecipePictureShouldBeFound("label.notEquals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    void getAllRecipePicturesByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        // Get all the recipePictureList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultRecipePictureShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the recipePictureList where label equals to UPDATED_LABEL
        defaultRecipePictureShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    void getAllRecipePicturesByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        // Get all the recipePictureList where label is not null
        defaultRecipePictureShouldBeFound("label.specified=true");

        // Get all the recipePictureList where label is null
        defaultRecipePictureShouldNotBeFound("label.specified=false");
    }

    @Test
    @Transactional
    void getAllRecipePicturesByLabelContainsSomething() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        // Get all the recipePictureList where label contains DEFAULT_LABEL
        defaultRecipePictureShouldBeFound("label.contains=" + DEFAULT_LABEL);

        // Get all the recipePictureList where label contains UPDATED_LABEL
        defaultRecipePictureShouldNotBeFound("label.contains=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    void getAllRecipePicturesByLabelNotContainsSomething() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        // Get all the recipePictureList where label does not contain DEFAULT_LABEL
        defaultRecipePictureShouldNotBeFound("label.doesNotContain=" + DEFAULT_LABEL);

        // Get all the recipePictureList where label does not contain UPDATED_LABEL
        defaultRecipePictureShouldBeFound("label.doesNotContain=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    void getAllRecipePicturesByOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        // Get all the recipePictureList where order equals to DEFAULT_ORDER
        defaultRecipePictureShouldBeFound("order.equals=" + DEFAULT_ORDER);

        // Get all the recipePictureList where order equals to UPDATED_ORDER
        defaultRecipePictureShouldNotBeFound("order.equals=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    void getAllRecipePicturesByOrderIsNotEqualToSomething() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        // Get all the recipePictureList where order not equals to DEFAULT_ORDER
        defaultRecipePictureShouldNotBeFound("order.notEquals=" + DEFAULT_ORDER);

        // Get all the recipePictureList where order not equals to UPDATED_ORDER
        defaultRecipePictureShouldBeFound("order.notEquals=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    void getAllRecipePicturesByOrderIsInShouldWork() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        // Get all the recipePictureList where order in DEFAULT_ORDER or UPDATED_ORDER
        defaultRecipePictureShouldBeFound("order.in=" + DEFAULT_ORDER + "," + UPDATED_ORDER);

        // Get all the recipePictureList where order equals to UPDATED_ORDER
        defaultRecipePictureShouldNotBeFound("order.in=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    void getAllRecipePicturesByOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        // Get all the recipePictureList where order is not null
        defaultRecipePictureShouldBeFound("order.specified=true");

        // Get all the recipePictureList where order is null
        defaultRecipePictureShouldNotBeFound("order.specified=false");
    }

    @Test
    @Transactional
    void getAllRecipePicturesByOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        // Get all the recipePictureList where order is greater than or equal to DEFAULT_ORDER
        defaultRecipePictureShouldBeFound("order.greaterThanOrEqual=" + DEFAULT_ORDER);

        // Get all the recipePictureList where order is greater than or equal to UPDATED_ORDER
        defaultRecipePictureShouldNotBeFound("order.greaterThanOrEqual=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    void getAllRecipePicturesByOrderIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        // Get all the recipePictureList where order is less than or equal to DEFAULT_ORDER
        defaultRecipePictureShouldBeFound("order.lessThanOrEqual=" + DEFAULT_ORDER);

        // Get all the recipePictureList where order is less than or equal to SMALLER_ORDER
        defaultRecipePictureShouldNotBeFound("order.lessThanOrEqual=" + SMALLER_ORDER);
    }

    @Test
    @Transactional
    void getAllRecipePicturesByOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        // Get all the recipePictureList where order is less than DEFAULT_ORDER
        defaultRecipePictureShouldNotBeFound("order.lessThan=" + DEFAULT_ORDER);

        // Get all the recipePictureList where order is less than UPDATED_ORDER
        defaultRecipePictureShouldBeFound("order.lessThan=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    void getAllRecipePicturesByOrderIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        // Get all the recipePictureList where order is greater than DEFAULT_ORDER
        defaultRecipePictureShouldNotBeFound("order.greaterThan=" + DEFAULT_ORDER);

        // Get all the recipePictureList where order is greater than SMALLER_ORDER
        defaultRecipePictureShouldBeFound("order.greaterThan=" + SMALLER_ORDER);
    }

    @Test
    @Transactional
    void getAllRecipePicturesByRecipeIsEqualToSomething() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);
        Recipe recipe;
        if (TestUtil.findAll(em, Recipe.class).isEmpty()) {
            recipe = RecipeResourceIT.createEntity(em);
            em.persist(recipe);
            em.flush();
        } else {
            recipe = TestUtil.findAll(em, Recipe.class).get(0);
        }
        em.persist(recipe);
        em.flush();
        recipePicture.addRecipe(recipe);
        recipePictureRepository.saveAndFlush(recipePicture);
        Long recipeId = recipe.getId();

        // Get all the recipePictureList where recipe equals to recipeId
        defaultRecipePictureShouldBeFound("recipeId.equals=" + recipeId);

        // Get all the recipePictureList where recipe equals to (recipeId + 1)
        defaultRecipePictureShouldNotBeFound("recipeId.equals=" + (recipeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRecipePictureShouldBeFound(String filter) throws Exception {
        restRecipePictureMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recipePicture.getId().intValue())))
            .andExpect(jsonPath("$.[*].pictureContentType").value(hasItem(DEFAULT_PICTURE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].picture").value(hasItem(Base64Utils.encodeToString(DEFAULT_PICTURE))))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER)));

        // Check, that the count call also returns 1
        restRecipePictureMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRecipePictureShouldNotBeFound(String filter) throws Exception {
        restRecipePictureMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRecipePictureMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingRecipePicture() throws Exception {
        // Get the recipePicture
        restRecipePictureMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewRecipePicture() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        int databaseSizeBeforeUpdate = recipePictureRepository.findAll().size();

        // Update the recipePicture
        RecipePicture updatedRecipePicture = recipePictureRepository.findById(recipePicture.getId()).get();
        // Disconnect from session so that the updates on updatedRecipePicture are not directly saved in db
        em.detach(updatedRecipePicture);
        updatedRecipePicture
            .picture(UPDATED_PICTURE)
            .pictureContentType(UPDATED_PICTURE_CONTENT_TYPE)
            .label(UPDATED_LABEL)
            .order(UPDATED_ORDER);
        RecipePictureDTO recipePictureDTO = recipePictureMapper.toDto(updatedRecipePicture);

        restRecipePictureMockMvc
            .perform(
                put(ENTITY_API_URL_ID, recipePictureDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(recipePictureDTO))
            )
            .andExpect(status().isOk());

        // Validate the RecipePicture in the database
        List<RecipePicture> recipePictureList = recipePictureRepository.findAll();
        assertThat(recipePictureList).hasSize(databaseSizeBeforeUpdate);
        RecipePicture testRecipePicture = recipePictureList.get(recipePictureList.size() - 1);
        assertThat(testRecipePicture.getPicture()).isEqualTo(UPDATED_PICTURE);
        assertThat(testRecipePicture.getPictureContentType()).isEqualTo(UPDATED_PICTURE_CONTENT_TYPE);
        assertThat(testRecipePicture.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testRecipePicture.getOrder()).isEqualTo(UPDATED_ORDER);

        // Validate the RecipePicture in Elasticsearch
        verify(mockRecipePictureSearchRepository).save(testRecipePicture);
    }

    @Test
    @Transactional
    void putNonExistingRecipePicture() throws Exception {
        int databaseSizeBeforeUpdate = recipePictureRepository.findAll().size();
        recipePicture.setId(count.incrementAndGet());

        // Create the RecipePicture
        RecipePictureDTO recipePictureDTO = recipePictureMapper.toDto(recipePicture);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRecipePictureMockMvc
            .perform(
                put(ENTITY_API_URL_ID, recipePictureDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(recipePictureDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RecipePicture in the database
        List<RecipePicture> recipePictureList = recipePictureRepository.findAll();
        assertThat(recipePictureList).hasSize(databaseSizeBeforeUpdate);

        // Validate the RecipePicture in Elasticsearch
        verify(mockRecipePictureSearchRepository, times(0)).save(recipePicture);
    }

    @Test
    @Transactional
    void putWithIdMismatchRecipePicture() throws Exception {
        int databaseSizeBeforeUpdate = recipePictureRepository.findAll().size();
        recipePicture.setId(count.incrementAndGet());

        // Create the RecipePicture
        RecipePictureDTO recipePictureDTO = recipePictureMapper.toDto(recipePicture);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRecipePictureMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(recipePictureDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RecipePicture in the database
        List<RecipePicture> recipePictureList = recipePictureRepository.findAll();
        assertThat(recipePictureList).hasSize(databaseSizeBeforeUpdate);

        // Validate the RecipePicture in Elasticsearch
        verify(mockRecipePictureSearchRepository, times(0)).save(recipePicture);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamRecipePicture() throws Exception {
        int databaseSizeBeforeUpdate = recipePictureRepository.findAll().size();
        recipePicture.setId(count.incrementAndGet());

        // Create the RecipePicture
        RecipePictureDTO recipePictureDTO = recipePictureMapper.toDto(recipePicture);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRecipePictureMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(recipePictureDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RecipePicture in the database
        List<RecipePicture> recipePictureList = recipePictureRepository.findAll();
        assertThat(recipePictureList).hasSize(databaseSizeBeforeUpdate);

        // Validate the RecipePicture in Elasticsearch
        verify(mockRecipePictureSearchRepository, times(0)).save(recipePicture);
    }

    @Test
    @Transactional
    void partialUpdateRecipePictureWithPatch() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        int databaseSizeBeforeUpdate = recipePictureRepository.findAll().size();

        // Update the recipePicture using partial update
        RecipePicture partialUpdatedRecipePicture = new RecipePicture();
        partialUpdatedRecipePicture.setId(recipePicture.getId());

        partialUpdatedRecipePicture
            .picture(UPDATED_PICTURE)
            .pictureContentType(UPDATED_PICTURE_CONTENT_TYPE)
            .label(UPDATED_LABEL)
            .order(UPDATED_ORDER);

        restRecipePictureMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRecipePicture.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRecipePicture))
            )
            .andExpect(status().isOk());

        // Validate the RecipePicture in the database
        List<RecipePicture> recipePictureList = recipePictureRepository.findAll();
        assertThat(recipePictureList).hasSize(databaseSizeBeforeUpdate);
        RecipePicture testRecipePicture = recipePictureList.get(recipePictureList.size() - 1);
        assertThat(testRecipePicture.getPicture()).isEqualTo(UPDATED_PICTURE);
        assertThat(testRecipePicture.getPictureContentType()).isEqualTo(UPDATED_PICTURE_CONTENT_TYPE);
        assertThat(testRecipePicture.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testRecipePicture.getOrder()).isEqualTo(UPDATED_ORDER);
    }

    @Test
    @Transactional
    void fullUpdateRecipePictureWithPatch() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        int databaseSizeBeforeUpdate = recipePictureRepository.findAll().size();

        // Update the recipePicture using partial update
        RecipePicture partialUpdatedRecipePicture = new RecipePicture();
        partialUpdatedRecipePicture.setId(recipePicture.getId());

        partialUpdatedRecipePicture
            .picture(UPDATED_PICTURE)
            .pictureContentType(UPDATED_PICTURE_CONTENT_TYPE)
            .label(UPDATED_LABEL)
            .order(UPDATED_ORDER);

        restRecipePictureMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRecipePicture.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRecipePicture))
            )
            .andExpect(status().isOk());

        // Validate the RecipePicture in the database
        List<RecipePicture> recipePictureList = recipePictureRepository.findAll();
        assertThat(recipePictureList).hasSize(databaseSizeBeforeUpdate);
        RecipePicture testRecipePicture = recipePictureList.get(recipePictureList.size() - 1);
        assertThat(testRecipePicture.getPicture()).isEqualTo(UPDATED_PICTURE);
        assertThat(testRecipePicture.getPictureContentType()).isEqualTo(UPDATED_PICTURE_CONTENT_TYPE);
        assertThat(testRecipePicture.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testRecipePicture.getOrder()).isEqualTo(UPDATED_ORDER);
    }

    @Test
    @Transactional
    void patchNonExistingRecipePicture() throws Exception {
        int databaseSizeBeforeUpdate = recipePictureRepository.findAll().size();
        recipePicture.setId(count.incrementAndGet());

        // Create the RecipePicture
        RecipePictureDTO recipePictureDTO = recipePictureMapper.toDto(recipePicture);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRecipePictureMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, recipePictureDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(recipePictureDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RecipePicture in the database
        List<RecipePicture> recipePictureList = recipePictureRepository.findAll();
        assertThat(recipePictureList).hasSize(databaseSizeBeforeUpdate);

        // Validate the RecipePicture in Elasticsearch
        verify(mockRecipePictureSearchRepository, times(0)).save(recipePicture);
    }

    @Test
    @Transactional
    void patchWithIdMismatchRecipePicture() throws Exception {
        int databaseSizeBeforeUpdate = recipePictureRepository.findAll().size();
        recipePicture.setId(count.incrementAndGet());

        // Create the RecipePicture
        RecipePictureDTO recipePictureDTO = recipePictureMapper.toDto(recipePicture);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRecipePictureMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(recipePictureDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RecipePicture in the database
        List<RecipePicture> recipePictureList = recipePictureRepository.findAll();
        assertThat(recipePictureList).hasSize(databaseSizeBeforeUpdate);

        // Validate the RecipePicture in Elasticsearch
        verify(mockRecipePictureSearchRepository, times(0)).save(recipePicture);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamRecipePicture() throws Exception {
        int databaseSizeBeforeUpdate = recipePictureRepository.findAll().size();
        recipePicture.setId(count.incrementAndGet());

        // Create the RecipePicture
        RecipePictureDTO recipePictureDTO = recipePictureMapper.toDto(recipePicture);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRecipePictureMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(recipePictureDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RecipePicture in the database
        List<RecipePicture> recipePictureList = recipePictureRepository.findAll();
        assertThat(recipePictureList).hasSize(databaseSizeBeforeUpdate);

        // Validate the RecipePicture in Elasticsearch
        verify(mockRecipePictureSearchRepository, times(0)).save(recipePicture);
    }

    @Test
    @Transactional
    void deleteRecipePicture() throws Exception {
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);

        int databaseSizeBeforeDelete = recipePictureRepository.findAll().size();

        // Delete the recipePicture
        restRecipePictureMockMvc
            .perform(delete(ENTITY_API_URL_ID, recipePicture.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RecipePicture> recipePictureList = recipePictureRepository.findAll();
        assertThat(recipePictureList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the RecipePicture in Elasticsearch
        verify(mockRecipePictureSearchRepository, times(1)).deleteById(recipePicture.getId());
    }

    @Test
    @Transactional
    void searchRecipePicture() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        recipePictureRepository.saveAndFlush(recipePicture);
        when(mockRecipePictureSearchRepository.search("id:" + recipePicture.getId(), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(recipePicture), PageRequest.of(0, 1), 1));

        // Search the recipePicture
        restRecipePictureMockMvc
            .perform(get(ENTITY_SEARCH_API_URL + "?query=id:" + recipePicture.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recipePicture.getId().intValue())))
            .andExpect(jsonPath("$.[*].pictureContentType").value(hasItem(DEFAULT_PICTURE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].picture").value(hasItem(Base64Utils.encodeToString(DEFAULT_PICTURE))))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER)));
    }
}

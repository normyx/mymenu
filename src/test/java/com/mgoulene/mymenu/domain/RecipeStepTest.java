package com.mgoulene.mymenu.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mgoulene.mymenu.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class RecipeStepTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RecipeStep.class);
        RecipeStep recipeStep1 = new RecipeStep();
        recipeStep1.setId(1L);
        RecipeStep recipeStep2 = new RecipeStep();
        recipeStep2.setId(recipeStep1.getId());
        assertThat(recipeStep1).isEqualTo(recipeStep2);
        recipeStep2.setId(2L);
        assertThat(recipeStep1).isNotEqualTo(recipeStep2);
        recipeStep1.setId(null);
        assertThat(recipeStep1).isNotEqualTo(recipeStep2);
    }
}

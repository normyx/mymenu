package com.mgoulene.mymenu.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mgoulene.mymenu.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class RecipePictureTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RecipePicture.class);
        RecipePicture recipePicture1 = new RecipePicture();
        recipePicture1.setId(1L);
        RecipePicture recipePicture2 = new RecipePicture();
        recipePicture2.setId(recipePicture1.getId());
        assertThat(recipePicture1).isEqualTo(recipePicture2);
        recipePicture2.setId(2L);
        assertThat(recipePicture1).isNotEqualTo(recipePicture2);
        recipePicture1.setId(null);
        assertThat(recipePicture1).isNotEqualTo(recipePicture2);
    }
}

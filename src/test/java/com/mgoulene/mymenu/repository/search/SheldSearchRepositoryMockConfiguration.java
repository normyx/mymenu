package com.mgoulene.mymenu.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link SheldSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class SheldSearchRepositoryMockConfiguration {

    @MockBean
    private SheldSearchRepository mockSheldSearchRepository;
}

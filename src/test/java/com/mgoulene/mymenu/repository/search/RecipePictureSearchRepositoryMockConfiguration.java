package com.mgoulene.mymenu.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link RecipePictureSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class RecipePictureSearchRepositoryMockConfiguration {

    @MockBean
    private RecipePictureSearchRepository mockRecipePictureSearchRepository;
}

package com.mgoulene.mymenu.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.mgoulene.mymenu.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class RecipePictureDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RecipePictureDTO.class);
        RecipePictureDTO recipePictureDTO1 = new RecipePictureDTO();
        recipePictureDTO1.setId(1L);
        RecipePictureDTO recipePictureDTO2 = new RecipePictureDTO();
        assertThat(recipePictureDTO1).isNotEqualTo(recipePictureDTO2);
        recipePictureDTO2.setId(recipePictureDTO1.getId());
        assertThat(recipePictureDTO1).isEqualTo(recipePictureDTO2);
        recipePictureDTO2.setId(2L);
        assertThat(recipePictureDTO1).isNotEqualTo(recipePictureDTO2);
        recipePictureDTO1.setId(null);
        assertThat(recipePictureDTO1).isNotEqualTo(recipePictureDTO2);
    }
}

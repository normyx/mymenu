package com.mgoulene.mymenu.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.mgoulene.mymenu.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class SheldDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SheldDTO.class);
        SheldDTO sheldDTO1 = new SheldDTO();
        sheldDTO1.setId(1L);
        SheldDTO sheldDTO2 = new SheldDTO();
        assertThat(sheldDTO1).isNotEqualTo(sheldDTO2);
        sheldDTO2.setId(sheldDTO1.getId());
        assertThat(sheldDTO1).isEqualTo(sheldDTO2);
        sheldDTO2.setId(2L);
        assertThat(sheldDTO1).isNotEqualTo(sheldDTO2);
        sheldDTO1.setId(null);
        assertThat(sheldDTO1).isNotEqualTo(sheldDTO2);
    }
}
